package com.heima.thread;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ThreadPoolTaskExecutorTest {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Test
    public void test(){
        //向线程池中提交100个任务
        for (int i = 0; i <100 ; i++) {
            threadPoolTaskExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("ThreadPoolTaskExecutor test 测试："+Thread.currentThread().getName());
                }
            });
        }
        //ThreadPoolTaskExecutor支持对线程池参数的ioc配置
        System.out.println("核心线程数："+threadPoolTaskExecutor.getCorePoolSize());
        System.out.println("最大线程数："+threadPoolTaskExecutor.getMaxPoolSize());
        System.out.println("线程等待超时时间："+threadPoolTaskExecutor.getKeepAliveSeconds());
        System.out.println("当前活跃的线程数："+threadPoolTaskExecutor.getActiveCount());
        System.out.println("线程池内线程的名称前缀："+threadPoolTaskExecutor.getThreadNamePrefix());
    }
}