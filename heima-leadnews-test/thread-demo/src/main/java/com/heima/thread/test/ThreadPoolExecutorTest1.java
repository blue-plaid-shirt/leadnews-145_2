package com.heima.thread.test;

import java.util.concurrent.*;

/**
 * 测试线程池
 */
public class ThreadPoolExecutorTest1 {
    public static void main(String[] args) throws Exception {
        /**
         * 创建线程池
         * 参数1表示定义的核心线程数量
         * 参数2表示池中允许的最大线程数量
         * 参数3表示线程存活时间
         * 参数4表示时间单位
         * 参数5表示用于在执行任务之前保存任务的队列
         */

        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5,
                100,60, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());
      
      //执行任务并且获取返回值
        Future<String> future = threadPool.submit(new myCallable("黑马程序员"));
      //  String result = future.get();立刻获取结果集
        String result = future.get(5, TimeUnit.SECONDS);//5秒钟以后获取
        System.out.println(result);
    }
    /**
     * 创建callable接口实现
     */
   public static class myCallable implements Callable<String> {
        //定义名称
        private String name;

        public myCallable(String name) {
            this.name = name;
        }

        @Override
        public String call() throws Exception {
            System.out.println("execute my callable task " + Thread.currentThread().getName());
            return "hello " + name;
        }
    }

}