package com.heima.thread.test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * 测试线程池
 */
public class ThreadPoolExecutorTest {
    public static void main(String[] args) throws Exception {
         /**
         * 创建线程池
         * 参数1表示定义的核心线程数量 5
         * 参数2表示池中允许的最大线程数量
         * 参数3表示线程存活时间
         * 参数4表示时间单位
         * 参数5表示用于在执行任务之前保存任务的队列
         */
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(5,
                100,60, TimeUnit.SECONDS,
                new LinkedBlockingQueue<Runnable>());
        //向线程池提交100任务，但是拿不到结果
        for(int i=0;i<101;i++){
            threadPool.execute(new MyTask(i));
        }
    }

    /**
     * 创建任务
     */
    public  static class  MyTask implements Runnable{
        //任务序号
        private int taskNo;
        public MyTask(int taskNo){
            this.taskNo = taskNo;
        }

        @Override
        public void run() {
            System.out.println("execute my task "+taskNo+"-"+Thread.currentThread().getName());
        }
    }
}