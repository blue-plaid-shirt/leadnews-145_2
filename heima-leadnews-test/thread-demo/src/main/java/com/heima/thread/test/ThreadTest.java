package com.heima.thread.test;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 普通的类
 */
public class ThreadTest {

    public static void main(String[] args) {
        //1.第一种方式
        myThread myThread1=new myThread();
        myThread1.setName("myThread-1");
        myThread1.start();


        myThread myThread2=new myThread();
        myThread2.setName("myThread-2");
        myThread2.start();

        //2.第二种方式
        myRunnable myRunnable1=new myRunnable();
       Thread thread1=new Thread(myRunnable1);
       thread1.setName("myRunnable-1");
       thread1.start();

        //main方法的线程
        System.out.println(Thread.currentThread().getName());





    }

    /**
     *创建线程第一种方式
     */
    public  static class myThread extends Thread{

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName()+"--"+System.currentTimeMillis());
        }


    }

    /**
     *创建线程第一种方式
     */
    public  static class myRunnable implements  Runnable{

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName()+"--"+System.currentTimeMillis());
        }
    }

}
