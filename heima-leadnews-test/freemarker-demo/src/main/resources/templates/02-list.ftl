<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>

<#-- list 数据的展示 -->
<b>展示list中的stu数据:</b>
<br>
<br>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>

<#list stus as stu>
    <#if stu.name='王子文'>
        <tr style="color: green">
            <td>${stu_index+1}</td>
            <td>${stu.name}</td>
            <td>${stu.age}</td>
            <td>${stu.money}</td>
        </tr>
        <#else >
            <tr>
                <td>${stu_index+1}</td>
                <td>${stu.name}</td>
                <td>${stu.age}</td>
                <td>${stu.money}</td>
            </tr>
    </#if>

</#list>

</table>
<hr>

<#-- Map 数据的展示 -->
<b>map数据的展示：</b>
<br/><br/>
<a href="###">方式一：通过map['keyname'].property</a><br/>
输出stu1的学生信息：<br/>

姓名：${resultMap['stu1'].name}<br/>
年龄：${resultMap['stu1'].age}<br/>

<br/>
<a href="###">方式二：通过map.keyname.property</a><br/>
输出stu2的学生信息：<br/>
姓名：${resultMap.stu2.name}<br/>
年龄：${resultMap.stu2.age}<br/>

<br/>
<a href="###">遍历map中两个学生信息：</a><br/>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
        <td>key</td>
        <td>value</td>
    </tr>
<#list resultMap?keys as key>
    <tr>
        <td>${key_index+1}</td>
        <td>${resultMap['${key}'].name}</td>
        <td>${resultMap['${key}'].age}</td>
        <td>${resultMap['${key}'].money}</td>
        <td>${key}</td>
        <td>${resultMap['${key}']}</td>
    </tr>
</#list>
</table>
<hr>

</body>
</html>