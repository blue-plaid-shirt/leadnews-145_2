package com.heima.freemarker.controller;

import com.heima.freemarker.pojos.Student;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller//注意，此时不可以写@RestController
@RequestMapping("/hello")
public class HelloController {

    /**
     *  快速入门案例
     *  利用model接口进行传参
     */
    @GetMapping
    public String hello(Model model){
        //数据传输
        model.addAttribute("name","二狗子");
        //跳转页面
        return "01-basic";
    }

    /**
     * 利用modelMap 类进行传参
     * @param modelMap
     * @return
     */
    @GetMapping("/mv")
    public String hello2(ModelMap modelMap){
        //数据传输
        modelMap.addAttribute("name","二狗子");
        modelMap.put("name1","小花");
        //跳转页面
        return "01-basic";
    }

    /**
     * 利用modelandview
     * @param
     * @return
     */
    @GetMapping("/mv2")
    public ModelAndView hello3(){
       ModelAndView mav=new ModelAndView("01-basic");

       //传输参数
        mav.addObject("name","隔壁老王");
        mav.addObject("name1","王子文");

        //传输对象参数
        Student student=new Student();
        student.setName("隔壁老李");
        student.setAge(18);
        mav.addObject("stu",student);

        //跳转页面
       // mav.setViewName("01-basic");
        return mav;
    }

    /**
     * list集合遍历
     * @param modelMap
     * @return
     */
    @GetMapping("/list")
    public String list(ModelMap modelMap){
        //创建一个list集合
        List<Student> list=new ArrayList<>();
        Student stu1=new Student();
        stu1.setName("王子文");
        stu1.setAge(24);
        stu1.setMoney(100f);

        Student stu2=new Student();
        stu2.setName("王子文2");
        stu2.setAge(23);
        stu2.setMoney(200f);

        list.add(stu1);
        list.add(stu2);

        //传输数据
        modelMap.put("stus",list);


        //map
        Map resultMap=new HashMap();

        resultMap.put("stu1",stu1);
        resultMap.put("stu2",stu2);

        //传输数据
        modelMap.put("resultMap",resultMap);




        //跳转页面
        return "02-list";
    }


}
