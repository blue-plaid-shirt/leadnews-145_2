package com.heima.freemarker;

import com.heima.freemarker.pojos.Student;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class FreeMarkerTest {

    @Autowired
    private Configuration configuration;

    /**
     * 用模板文件+数据模型生成静态html文件并保存在磁盘中
     */
    @Test
    public void createGenerHtml() throws Exception {
        //1.引入模板文件
        Template template = configuration.getTemplate("02-list.ftl");

        //2.获取数据模型
        Map dataModel=getDataModel();
        //3.生成静态文件
        template.process(dataModel, new FileWriter("E:\\list.html"));
    }
    private Map getDataModel(){
        Map dataMap=new HashMap();

        //创建一个list集合
        List<Student> list=new ArrayList<>();
        Student stu1=new Student();
        stu1.setName("王子文");
        stu1.setAge(24);
        stu1.setMoney(100f);

        Student stu2=new Student();
        stu2.setName("王子文2");
        stu2.setAge(23);
        stu2.setMoney(200f);

        list.add(stu1);
        list.add(stu2);

        //传输数据
        dataMap.put("stus",list);
        //map
        Map resultMap=new HashMap();

        resultMap.put("stu1",stu1);
        resultMap.put("stu2",stu2);
        //传输数据
        dataMap.put("resultMap",resultMap);

        return dataMap;

    }
}
