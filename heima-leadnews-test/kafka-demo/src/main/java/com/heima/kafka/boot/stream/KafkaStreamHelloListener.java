package com.heima.kafka.boot.stream;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;
import java.util.Arrays;

/**
 * 流式处理的类
 *
 */
@Configuration//配置类
public class KafkaStreamHelloListener {
    /**
     * 流式计算的方法，并且交给ioc管理
     * @param streamsBuilder
     * @return
     */
    @Bean
    public KStream<String,String> kStream(StreamsBuilder streamsBuilder){
//接收kafka中的消息
        KStream<String, String> kStream = streamsBuilder.stream("topic1");

        //处理消息
        kStream.flatMapValues(new ValueMapper<String, Iterable<String>>() {
            /**
             * 初始化计算
             * value表示发送消息的内容  hello kafka
             * 返回:["hello","kafka","hello","itcast",....]
             */
            @Override
            public Iterable<String> apply(String value) {
                System.out.println("消息内容:"+value);
                return Arrays.asList(value.split(" "));
            }
        })
                //分组查询根据value
                .groupBy((key,value)->value)
                .windowedBy(TimeWindows.of(Duration.ofSeconds(10)))//表示时间窗口，每10秒钟统计一次
                .count()//求和
                .toStream()//由ktable类型转成kstream类型
                //发送整理最终结果
                .map(new KeyValueMapper<Windowed<String>, Long, KeyValue<String,String>>() {
                    /**
                     * key表示具体的单词hello
                     * value表示当前这个单词所对应的次数
                     */
                    @Override
                    public KeyValue<String, String> apply(Windowed<String> key, Long value) {
                        return new KeyValue<>(key.key().toString(),value.toString());
                    }
                })
                //3.发送消息到kafka中
                .to("topic2");

        return kStream;
    }
}
