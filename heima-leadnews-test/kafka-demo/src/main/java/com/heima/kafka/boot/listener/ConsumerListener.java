package com.heima.kafka.boot.listener;

import com.alibaba.fastjson.JSON;
import com.heima.kafka.boot.pojos.User;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class ConsumerListener {

    //设置监听方法
    @KafkaListener(topics = {"topic2"})
    public void getMessage(ConsumerRecord consumerRecord){
        System.out.println(consumerRecord.key()+":"+consumerRecord.value());
    }
}
