package com.heima.kafka.simple;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.TopicPartition;

import java.time.Duration;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Properties;

/**
 * 消费者入门
 */
public class ConsumerStart {

    public static void main(String[] args) {
        //2.连接kafka信息
        Properties properties=new Properties();
        //连接kafka
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.200.145:9092");
        //设置序列化
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringDeserializer");
        //设置组id,必须要设置该属性，值可以随意起
        properties.put(ConsumerConfig.GROUP_ID_CONFIG,"group2");

        //properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG,"false");

        //1.创建消费者对象
        KafkaConsumer<String,String > kafkaConsumer=new KafkaConsumer<String, String>(properties);

        //3.订阅主题
        kafkaConsumer.subscribe(Collections.singleton("topic2"));

        //4.获取消息
        while (true){
            ConsumerRecords<String, String> consumerRecords = kafkaConsumer.poll(Duration.ofMillis(1000));//每1秒钟拉取一次
            for (ConsumerRecord<String, String> consumerRecord : consumerRecords) {
                System.out.println("消息key:"+consumerRecord.key()+"----"+"消息的内容:"+consumerRecord.value());
              /*  System.out.println("消息key:"+consumerRecord.key()+"----"+"消息的内容:"+consumerRecord.value());
                System.out.println("消息的偏移量也叫消息存储的序列号:"+consumerRecord.offset());
                //同步提交
                //kafkaConsumer.commitSync();

                //异步提交
                kafkaConsumer.commitAsync(new OffsetCommitCallback() {
                    @Override
                    public void onComplete(Map<TopicPartition, OffsetAndMetadata> offsets, Exception exception) {
                        System.out.println("异步提交");
                    }
                });*/

            }
        }

    }
}
