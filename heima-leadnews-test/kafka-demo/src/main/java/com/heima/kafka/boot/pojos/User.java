package com.heima.kafka.boot.pojos;

/**
 * @author ldp
 * @date 2021/12/18 16:26
 */
public class User {
    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
