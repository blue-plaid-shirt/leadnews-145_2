package com.heima.kafka.boot.controller;

import com.alibaba.fastjson.JSON;
import com.heima.kafka.boot.pojos.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.concurrent.Future;

@RestController
@RequestMapping("/api/v1/producer")
public class ProducerController {


    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @GetMapping
    public String send(){
        for (int i = 0; i <5 ; i++) {
            if (i % 2 == 0) {
                //发送消息
                kafkaTemplate.send("topic1", "hello kafka");
            }else{
                //发送消息
                kafkaTemplate.send("topic1", "hello itcast");
            }
        }

        return "发送成功";
    }
}
