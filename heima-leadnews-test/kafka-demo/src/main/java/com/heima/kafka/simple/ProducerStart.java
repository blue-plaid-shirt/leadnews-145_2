package com.heima.kafka.simple;

import org.apache.kafka.clients.producer.*;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 生产者的入门案例
 */
public class ProducerStart {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        //2.创建连接信息对象
        Properties properties=new Properties();
        //连接kafka
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.200.145:9092");
        //设置序列化
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,"org.apache.kafka.common.serialization.StringSerializer");
        //重试的机制配置
        properties.put(ProducerConfig.RETRIES_CONFIG,10);

        //1.创建生产者对象
        KafkaProducer<String,String> producer=new KafkaProducer<String, String>(properties);

        /**
         *  3.发送消息
         * ProducerRecord(String topic, K key, V value)
         * 参数1表示发送消息的队列名称
         * 参数2表示消息对应的key
         * 参数3表示发送的消息内容
         */
        for (int i = 0; i <5 ; i++) {
            if(i%2==0){
                ProducerRecord<String, String> record=new ProducerRecord<String,String>("topic1","first","hello kafka");
                //同步发送
                producer.send(record);
            }else{
                ProducerRecord<String, String> record=new ProducerRecord<String,String>("topic1","first","hello itcast");
                //同步发送
                producer.send(record);
            }
        }

       /* Future<RecordMetadata> future = producer.send(record);
        RecordMetadata metadata = future.get();
        int partition = metadata.partition();
        long offset = metadata.offset();
        String topic = metadata.topic();
        System.out.println("分区号:"+partition+",偏移量:"+offset+",队列名称:"+topic);*/


        //异步发送
      /*  producer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata metadata, Exception exception) {
                if(exception!=null){
                    throw new RuntimeException(exception);
                    //重试发送业务
                }
                int partition = metadata.partition();
                long offset = metadata.offset();
                String topic = metadata.topic();
                System.out.println("分区号:"+partition+",偏移量:"+offset+",队列名称:"+topic);
            }
        });*/


        //4.关闭流
        producer.close();
    }
}
