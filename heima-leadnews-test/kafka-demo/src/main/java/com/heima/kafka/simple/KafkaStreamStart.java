package com.heima.kafka.simple;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.kstream.*;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

/**
 * 流式处理的类
 */
public class KafkaStreamStart {
    public static void main(String[] args) {
        //2设置连接信息
        Properties props=new Properties();
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,"192.168.200.145:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        props.put(StreamsConfig.APPLICATION_ID_CONFIG,"applicationId");
        //3.流构建器
        StreamsBuilder streamsBuilder=new StreamsBuilder();

        //4.执行流式计算,其实就是为streamsBuilder构建器中添加参数
        streamProcessor(streamsBuilder);


        Topology topology = streamsBuilder.build();
        /**
         *  1.创建kafkastream对象
         *  KafkaStreams(final Topology topology,
         *                         final Properties props)
         *                         参数1表示 拓扑图
         *                         参数2表示属性，连接kafka信息
         */


        KafkaStreams kafkaStreams=new KafkaStreams(topology,props);

        //5.开启执行流
        kafkaStreams.start();
    }

    /**
     * 流式处理计算的业务
     * 需求：求单词个数（word count）
     *      生产者发送消息里面   hello kafka   hello itcast  hello kafka ....
     *      5次hello 3次kafka 2次itcast
     * @param streamsBuilder
     */
    private static void streamProcessor(StreamsBuilder streamsBuilder) {

        //接收kafka中的消息
        KStream<String, String> kStream = streamsBuilder.stream("topic1");

        //处理消息
        kStream.flatMapValues(new ValueMapper<String, Iterable<String>>() {
            /**
             * 初始化计算
             * value表示发送消息的内容  hello kafka
             * 返回:["hello","kafka","hello","itcast",....]
             */
            @Override
            public Iterable<String> apply(String value) {
                System.out.println("消息内容:"+value);
                return Arrays.asList(value.split(" "));
            }
        })
                //分组查询根据value
                .groupBy((key,value)->value)
                .windowedBy(TimeWindows.of(Duration.ofSeconds(10)))//表示时间窗口，每10秒钟统计一次
                .count()//求和
                .toStream()//由ktable类型转成kstream类型
                //发送整理最终结果
                .map(new KeyValueMapper<Windowed<String>, Long, KeyValue<String,String>>() {
                    /**
                     * key表示具体的单词hello
                     * value表示当前这个单词所对应的次数
                     */
                    @Override
                    public KeyValue<String, String> apply(Windowed<String> key, Long value) {
                        return new KeyValue<>(key.key().toString(),value.toString());
                    }
                })
                //3.发送消息到kafka中
                .to("topic2");
    }
















}
