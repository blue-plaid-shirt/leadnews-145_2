package com.heima;

import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.PutObjectArgs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class MinioTest {
    public static void main(String[] args) throws Exception {
        //3.获取上传文件资源
        InputStream stream=new FileInputStream("E:\\list.html");


        //1.创建minioclient客户端
        MinioClient minioClient = MinioClient.builder()
                .credentials("minio", "minio123")
                .endpoint("http://192.168.200.145:9000")
                .build();

        //2.上传文件

        PutObjectArgs putObjectArgs= PutObjectArgs.builder()
                .object("aa.html")//指定上传之后的文件名称
                .contentType("text/html")//文件类型
                .bucket("leadnews")//桶名
                .stream(stream,stream.available(),-1)
                .build();
        ObjectWriteResponse response = minioClient.putObject(putObjectArgs);
        System.out.println("http://192.168.200.145:9000/leadnews/aa.html");
    }
}
