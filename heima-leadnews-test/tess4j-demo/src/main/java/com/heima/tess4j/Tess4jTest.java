package com.heima.tess4j;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import java.awt.image.BufferedImage;
import java.io.File;

public class Tess4jTest {

    public static void main(String[] args) throws TesseractException {
        //3.创建文件
        File file=new File("C:\\Users\\45502\\Desktop\\aa.bmp");

        //1.声明对象tess4j
        ITesseract tesseract=new Tesseract();
        //4.设置语言
        tesseract.setLanguage("chi_sim");
        //5.设置语言所在的目录
        tesseract.setDatapath("E:\\tessdata");

        //2.执行OCR，抓取图片的文字返回
        String result = tesseract.doOCR(file);
        System.out.println(result);
    }
}
