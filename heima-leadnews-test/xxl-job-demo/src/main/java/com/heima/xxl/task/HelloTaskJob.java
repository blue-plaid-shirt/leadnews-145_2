package com.heima.xxl.task;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 执行任务的类
 */
@Component
public class HelloTaskJob {
    /**
     * 该方法执行的具体的任务是哪个
     * value值必须等于xxl-job-admin中的jobHandle的值
     */
   /* @XxlJob(value = "job-handle-145")
    public void executorTask(){
        System.out.println(new Date());
    }*/

    /**
     * 测试分片广播
     */
    @XxlJob("job-handle-145")
    public void shardingJobHandler(){
        //分片的参数
        int shardIndex = XxlJobHelper.getShardIndex();
        int shardTotal = XxlJobHelper.getShardTotal();

        //业务逻辑
        List<Integer> list = getList();
        for (Integer integer : list) {
            if(integer % shardTotal == shardIndex){
                System.out.println("当前第"+shardIndex+"分片执行了，任务项为："+integer);
            }
        }
    }

    public List<Integer> getList(){
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list.add(i);
        }
        return list;
    }
}
