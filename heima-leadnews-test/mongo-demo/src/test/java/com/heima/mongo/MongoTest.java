package com.heima.mongo;

import com.heima.mongo.pojos.Address;
import com.heima.mongo.pojos.Person;
import com.mongodb.client.result.DeleteResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MongoTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    /**
     * 新增文档   save
     * 如果发现mongo数据库中的id不存在的话，则执行新增操作
     * 如果发现id已经存在，则执行修改操作
     */
    @Test
    public void savePerson(){
        Person person=new Person(1L,"二狗子2222",13,new Address("湖北","武汉","黄陂"));
        mongoTemplate.save(person);
    }

    /**
     * 新增文档   insert
     * 只能是新增，主键不可以重复
     */
    @Test
    public void savePerson2(){
        Person person=new Person(2L,"二狗子",13,new Address("湖北","武汉","黄陂"));
        mongoTemplate.insert(person);
    }

    @Test
    public void delete(){
        Query query=Query.query(Criteria.where("id").is(1l));
        DeleteResult result = mongoTemplate.remove(query, Person.class);
    }
    //全查
    @Test
    public void query(){
        Query query=new Query();
        query.with(Sort.by(Sort.Direction.DESC,"age"));
        List<Person> personList = mongoTemplate.find(query, Person.class);
        System.out.println(personList);
    }
    //单一查询
    @Test
    public void queryByOne(){
        //Person person = mongoTemplate.findById(2l, Person.class);

        Person person = mongoTemplate.findOne(Query.query(Criteria.where("name").is("二狗子").and("age").is(12)),
                Person.class);
        System.out.println(person);
    }


    /**
     * 根据条件查询
     *
     * select * from 表 where id=1 and age=12
     */
    @Test
    public void queryTiaojian(){


        //and 和or   select * from 表 where id=1 and (age=12 or name=二狗子)
        Query query=Query.query(Criteria.where("id").is(1l).orOperator(Criteria.where("age").is(20),Criteria.where("name").is("二狗子")));

        List<Person> list = mongoTemplate.find(query, Person.class);
        System.out.println(list);
    }


}
