package com.heima.mongo.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(value = "person")//集合名称，类似于表名
public class Person {
    //id
    @Id //表示主键
    private Long id;
    //姓名
    private String name;
    //年龄
    private int age;
    //地址
    private Address address;
}