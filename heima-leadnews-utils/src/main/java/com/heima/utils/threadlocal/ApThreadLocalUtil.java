package com.heima.utils.threadlocal;

import com.heima.model.user.pojos.ApUser;
import com.heima.model.wemedia.pojos.WmUser;

/**
 * APP端的threadlocal工具类
 */
public class ApThreadLocalUtil {
    //创建threadlocal
    private static final ThreadLocal<ApUser> AP_THREAD_LOCAL=new ThreadLocal<>();

    public static  void setApUser(ApUser apUser){
        AP_THREAD_LOCAL.set(apUser);
    }

    public static   ApUser getApUser(){
        return AP_THREAD_LOCAL.get();
    }

    public static  void removeApUser(){
        AP_THREAD_LOCAL.remove();
    }
}
