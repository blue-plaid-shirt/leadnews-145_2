package com.heima.utils.threadlocal;

import com.heima.model.wemedia.pojos.WmUser;

/**
 * 自媒体端的threadlocal工具类
 */
public class WmThreadLocalUtil {
    //创建threadlocal
    private static final ThreadLocal<WmUser> WM_THREAD_LOCAL=new ThreadLocal<>();

    public static  void setWmUser(WmUser wmUser){
        WM_THREAD_LOCAL.set(wmUser);
    }

    public static   WmUser getWmUser(){
        return WM_THREAD_LOCAL.get();
    }

    public static  void removeWmUser(){
        WM_THREAD_LOCAL.remove();
    }
}
