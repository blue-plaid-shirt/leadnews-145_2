package com.heima.behavior.service.impl;

import com.heima.behavior.dtos.ArticleBehaviorDto;
import com.heima.behavior.service.ApBehaviorServcie;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ApBehaviorServcieImpl implements ApBehaviorServcie {

    @Autowired
    private StringRedisTemplate redisTemplate;
    /**
     * 文章关系展示
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult loadArticleBehavior(ArticleBehaviorDto dto) {
        //1.参数校验
        if (dto.getArticleId() == null || dto.getAuthorId()==null) {
            return ResponseResult.okResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.获取登录用户id
        ApUser apUser = ApThreadLocalUtil.getApUser();
        //表示未登录
        if(apUser==null){
            return ResponseResult.okResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //游客身份不能进行以下操作
        if(apUser.getId()==0){
            return ResponseResult.okResult(405,"游客身份禁止，需进行登录");
        }

        //定义这个变量
        boolean islike=false,isunlike=false,iscollection=false,isfollow=false,isforward=false;

        //查询是否关注过
        String followKey="USER_FOLLOW_"+apUser.getId();
        Boolean isfollowFlag = redisTemplate.opsForHash().hasKey(followKey, dto.getAuthorId().toString());
        if(isfollowFlag) isfollow=true;

        //是否点赞过
        String likesKey="USER_LIKES_"+dto.getArticleId();
        Boolean islikeFlag = redisTemplate.opsForHash().hasKey(likesKey, apUser.getId().toString());
        if(islikeFlag) islike=true;
        //是否不喜欢过 TODO

        //是否收藏过 TODO

        //是否转发过 TODO

        //封装数据
        Map map=new HashMap<>();
        map.put("islike",islike);
        map.put("isunlike",isunlike);
        map.put("iscollection",iscollection);
        map.put("isfollow",isfollow);
        map.put("isforward",isforward);

        return ResponseResult.okResult(map);
    }
}
