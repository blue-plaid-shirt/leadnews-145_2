package com.heima.behavior.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.behavior.dtos.ReadBehaviorDto;
import com.heima.behavior.pojos.ApReadBehavior;
import com.heima.behavior.service.ApReadBehaviorService;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ApReadBehaviorServiceImpl implements ApReadBehaviorService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    /**
     * 阅读行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult readBehavior(ReadBehaviorDto dto) {

        //1.校验参数
        if (dto.getArticleId() == null) {
            return ResponseResult.okResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.获取登录用户id
        ApUser apUser = ApThreadLocalUtil.getApUser();
        //表示未登录
        if(apUser==null){
            return ResponseResult.okResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //游客身份不能进行以下操作
        if(apUser.getId()==0){
            return ResponseResult.okResult(405,"游客身份禁止，需进行登录");
        }


        //判断当前mongo中是否存在已有的数据，如果存在，则更新次数，否则新增数据
        ApReadBehavior apReadBehavior = mongoTemplate.findOne(
                Query.query(Criteria.where("userId").is(apUser.getId()).and("articleId").is(dto.getArticleId())),
                ApReadBehavior.class
        );
        if(apReadBehavior!=null){
            //对次数进行累加
            apReadBehavior.setCount(apReadBehavior.getCount()+dto.getCount());
        }else{
            //新增
            apReadBehavior=new ApReadBehavior();
            BeanUtils.copyProperties(dto,apReadBehavior);
            apReadBehavior.setUserId(apUser.getId().longValue());
            apReadBehavior.setUpdatedTime(new Date());
            apReadBehavior.setCreatedTime(new Date());
            apReadBehavior.setCount(dto.getCount().intValue());
        }
        //更新mongo
        mongoTemplate.save(apReadBehavior);


        UpdateArticleMess mess=new UpdateArticleMess();
        mess.setType(UpdateArticleMess.UpdateArticleType.VIEWS);
        mess.setArticleId(dto.getArticleId());
        mess.setAdd(1);
        //发送消息内容
        kafkaTemplate.send(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC, JSON.toJSONString(mess));

        return ResponseResult.okResult("阅读行为完成");
    }
}
