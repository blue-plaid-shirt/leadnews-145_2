package com.heima.behavior.pojos;

import com.heima.model.common.annotation.IdEncrypt;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * APP关注行为表
 * </p>
 *
 * @author itheima
 */
@Data
@Document("ap_follow_behavior")
public class ApFollowBehavior implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 文章ID
     */
    @IdEncrypt
    private Long articleId;

    /**
     * 被关注人的ID，理解为作者id
     */
    private Integer followId;

    /**
     * 登录时间
     */
    private Date createdTime;

}