package com.heima.behavior.service;

import com.heima.behavior.dtos.FollowBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.RequestBody;

public interface UserFollowService {
    /**
     * 用户关注作者行为
     * @param dto
     * @return
     */
    public ResponseResult userFollow(FollowBehaviorDto dto);
}
