package com.heima.behavior.service;

import com.heima.behavior.dtos.ReadBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.RequestBody;

public interface ApReadBehaviorService {
    /**
     * 阅读行为
     * @param dto
     * @return
     */
    public ResponseResult readBehavior( ReadBehaviorDto dto);
}
