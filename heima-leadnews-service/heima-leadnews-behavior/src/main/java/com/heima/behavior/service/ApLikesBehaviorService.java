package com.heima.behavior.service;

import com.heima.behavior.dtos.LikesBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ApLikesBehaviorService {
    /**
     * 点赞行为
     * @param dto
     * @return
     */
    ResponseResult like(LikesBehaviorDto dto);
}
