package com.heima.behavior.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.behavior.dtos.LikesBehaviorDto;
import com.heima.behavior.pojos.ApFollowBehavior;
import com.heima.behavior.pojos.ApLikesBehavior;
import com.heima.behavior.service.ApLikesBehaviorService;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class ApLikesBehaviorServiceImpl implements ApLikesBehaviorService {


    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;


    /**
     * 点赞行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult like(LikesBehaviorDto dto) {
        //1.参数校验
        if (dto.getArticleId() == null) {
            return ResponseResult.okResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.获取登录用户id
        ApUser apUser = ApThreadLocalUtil.getApUser();
        if(apUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //排除游客身份
        if(apUser.getId()==0){
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_OPERATOR_AUTH,"游客身份是不可以关注用户");
        }

        //3.数据要同步到redis
        //定义点赞的的key
        String likesKey="USER_LIKES_"+dto.getArticleId();

        //4. 查询redis是否已经关注过
        Boolean likesFlag = stringRedisTemplate.opsForHash().hasKey(likesKey, apUser.getId().toString());



        UpdateArticleMess mess=new UpdateArticleMess();
        mess.setType(UpdateArticleMess.UpdateArticleType.LIKES);
        mess.setArticleId(dto.getArticleId());

        //如果当前用户已经点赞过该文章，并且现在执行的动作是点赞动作,不能继续以下的操作
        if(likesFlag && dto.getOperation()==0){
            return ResponseResult.okResult("您已经点赞过该文章，请勿重复点赞");
        }
        //如果当前用户已经点赞过该文章，并且现在执行的动作是取消点赞的动作，则删除redis和mongo
        if(likesFlag && dto.getOperation()==1){
            //1.删除redis
            stringRedisTemplate.opsForHash().delete(likesKey,apUser.getId().toString());
            //2.删除mongo
            mongoTemplate.remove(
                    Query.query(Criteria.where("userId").is(apUser.getId()).and("articleId").is(dto.getArticleId())),
                    ApLikesBehavior.class
            );

            mess.setAdd(-1);
        }
        //如果当前用户没有点赞过该文章,并且现在执行的动作是点赞动作，则新增redis和mongo
        if(!likesFlag && dto.getOperation()==0){
            //1.新增mongo
           ApLikesBehavior apLikesBehavior=new ApLikesBehavior();
            apLikesBehavior.setType(dto.getType());
            apLikesBehavior.setUserId(apUser.getId().longValue());
            apLikesBehavior.setCreatedTime(new Date());
            apLikesBehavior.setArticleId(dto.getArticleId());

            mongoTemplate.save(apLikesBehavior);

            //2.新增redis
            stringRedisTemplate.opsForHash().put(likesKey,apUser.getId().toString(),"1");

            mess.setAdd(1);
        }




        //发送消息内容
        kafkaTemplate.send(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC, JSON.toJSONString(mess));



        //返回数据
        return ResponseResult.okResult("点赞文章成功");
    }
}
