package com.heima.behavior.service.impl;

import com.heima.behavior.dtos.FollowBehaviorDto;
import com.heima.behavior.pojos.ApFollowBehavior;
import com.heima.behavior.service.UserFollowService;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserFollowServiceImpl implements UserFollowService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    /**
     * 用户关注作者行为
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult userFollow(FollowBehaviorDto dto) {
        //1.参数校验
        if (dto.getArticleId() == null || dto.getAuthorId() == null) {
            return ResponseResult.okResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        //2.获取登录用户id
        ApUser apUser = ApThreadLocalUtil.getApUser();
        if(apUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //排除游客身份
        if(apUser.getId()==0){
            return ResponseResult.errorResult(AppHttpCodeEnum.NO_OPERATOR_AUTH,"游客身份是不可以关注用户");
        }

        //3.数据要同步到redis
            //定义关注的的key
        String followKey="USER_FOLLOW_"+apUser.getId();
            //定义粉丝的key
        String fansKey="USER_FANS_"+dto.getAuthorId();

        //4. 查询redis是否已经关注过
        Boolean followFlag = stringRedisTemplate.opsForHash().hasKey(followKey, dto.getAuthorId().toString());

        //如果当前用户已经关注过该作者，并且现在执行的动作是关注动作,不能继续以下的操作
        if(followFlag && dto.getOperation()==0){
            return ResponseResult.okResult("您已经关注过该作者，请勿重复关注");
        }
        //如果当前已经关注过该作者，但是现在执行的是取消关注作者的动作，则删除redis和mongo
        if(followFlag && dto.getOperation()==1){
            //1.删除redis
            stringRedisTemplate.opsForHash().delete(followKey,dto.getAuthorId().toString());
            stringRedisTemplate.opsForHash().delete(fansKey,apUser.getId().toString());
            //2.删除mongo
            mongoTemplate.remove(
                    Query.query(Criteria.where("userId").is(apUser.getId()).and("followId").is(dto.getAuthorId())),
                    ApFollowBehavior.class
            );
        }
        //如果当前没有关注过该作者,现在执行的是关注作者的动作，则新增redis和mongo
        if(!followFlag && dto.getOperation()==0){
            //1.新增mongo
            ApFollowBehavior apFollowBehavior=new ApFollowBehavior();
            apFollowBehavior.setFollowId(dto.getAuthorId());//作者或者被关注人id
            apFollowBehavior.setUserId(apUser.getId().longValue());

            apFollowBehavior.setCreatedTime(new Date());
            apFollowBehavior.setArticleId(dto.getArticleId());

            mongoTemplate.save(apFollowBehavior);

            //2.新增redis
            stringRedisTemplate.opsForHash().put(followKey,dto.getAuthorId().toString(),"1");
            stringRedisTemplate.boundHashOps(fansKey).put(apUser.getId().toString(),"1");
        }
        //返回数据
        return ResponseResult.okResult("关注用户成功");
    }
}
