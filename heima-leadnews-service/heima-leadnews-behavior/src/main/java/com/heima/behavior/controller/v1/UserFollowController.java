package com.heima.behavior.controller.v1;

import com.heima.behavior.dtos.FollowBehaviorDto;
import com.heima.behavior.service.UserFollowService;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户关注行为
 */
@RestController
@RequestMapping("/api/v1/follow_behavior")
public class UserFollowController {

    @Autowired
    private UserFollowService userFollowService;

    /**
     * 用户关注作者行为
     * @param dto
     * @return
     */
    @PostMapping("/user_follow")
    public ResponseResult userFollow(@RequestBody FollowBehaviorDto dto){
        return userFollowService.userFollow(dto);
    }
}
