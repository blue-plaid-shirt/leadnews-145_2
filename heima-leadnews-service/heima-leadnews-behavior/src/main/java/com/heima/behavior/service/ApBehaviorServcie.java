package com.heima.behavior.service;

import com.heima.behavior.dtos.ArticleBehaviorDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.RequestBody;

public interface ApBehaviorServcie {
    /**
     * 文章关系展示
     * @param dto
     * @return
     */
    public ResponseResult loadArticleBehavior( ArticleBehaviorDto dto);
}
