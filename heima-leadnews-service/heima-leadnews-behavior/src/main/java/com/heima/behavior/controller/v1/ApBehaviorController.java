package com.heima.behavior.controller.v1;

import com.heima.behavior.dtos.ArticleBehaviorDto;
import com.heima.behavior.service.ApBehaviorServcie;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/behavior")
public class ApBehaviorController {

    @Autowired
    private ApBehaviorServcie apBehaviorServcie;

    /**
     * 文章关系展示
     * @param dto
     * @return
     */
    @PostMapping(path = "/load_article_behavior")
    public ResponseResult loadArticleBehavior(@RequestBody ArticleBehaviorDto dto){
        return apBehaviorServcie.loadArticleBehavior(dto);
    }
}
