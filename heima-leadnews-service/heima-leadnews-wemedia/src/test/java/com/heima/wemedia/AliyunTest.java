package com.heima.wemedia;

import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.file.service.FileStorageService;
import com.heima.wemedia.service.WmAutoScanService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest
@RunWith(SpringRunner.class)
public class AliyunTest {

    @Autowired
    private GreenTextScan textScan;
    @Autowired
    private GreenImageScan greenImageScan;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private WmAutoScanService wmAutoScanService;

    //文本检测
    @Test
    public void textScan() throws Exception {
        Map map = textScan.greeTextScan("我是谁，我玩冰毒");
        System.out.println(map);
    }

    //图片检测
   @Test
   public void imageScan() throws Exception {

       List<byte[]> imageList=new ArrayList<>();
        //2.下载图片
       byte[] bytes = fileStorageService.downLoadFile("http://192.168.200.145:9000/leadnews/2022/07/28/3f8f96986ba64c159848a421b39453d5.jpg");
       imageList.add(bytes);
       //1.调用图片审核的接口
       Map map = greenImageScan.imageScan(imageList);
       System.out.println(map);
   }

   //测试自动审核的方法
    @Test
    public void autoScanTest(){
        wmAutoScanService.autoScanWmNews(6235);
    }










}
