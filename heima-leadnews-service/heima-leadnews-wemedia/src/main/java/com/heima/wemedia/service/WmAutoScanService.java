package com.heima.wemedia.service;

/**
 * 自动审核业务
 */
public interface WmAutoScanService {
    /**
     * 文章审核
     * @param newsId 文章id
     */
    public void autoScanWmNews(Integer newsId);
}
