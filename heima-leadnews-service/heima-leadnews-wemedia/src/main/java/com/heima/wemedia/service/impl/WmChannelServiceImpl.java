package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.service.WmChannelService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WmChannelServiceImpl extends ServiceImpl<WmChannelMapper, WmChannel> implements WmChannelService {
    /**
     * 获取所有的频道列表
     *
     * @return
     */
    @Override
    public ResponseResult<List<WmChannel>> getChannelList() {
        List<WmChannel> wmChannels = list(Wrappers.<WmChannel>lambdaQuery()
        .eq(WmChannel::getStatus,true));
        return ResponseResult.okResult(wmChannels);
    }
}
