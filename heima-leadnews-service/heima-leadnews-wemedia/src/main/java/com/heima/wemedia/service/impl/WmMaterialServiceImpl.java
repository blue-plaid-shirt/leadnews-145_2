package com.heima.wemedia.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.file.service.FileStorageService;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.utils.threadlocal.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

@Service
public class WmMaterialServiceImpl extends ServiceImpl<WmMaterialMapper, WmMaterial> implements WmMaterialService {

    @Autowired
    private FileStorageService fileStorageService;


    /**
     * 图片上传
     *
     * @param file
     * @return
     */
    @Override
    public ResponseResult uploadImage(MultipartFile file) {

        try {
            //1.上传图片到minio中
            String originalFilename = file.getOriginalFilename();//获取文件的原始名称
            String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
            String filename=UUID.randomUUID().toString().replace("-","")+suffix;
            String path = fileStorageService.uploadImgFile("", filename, file.getInputStream());
            //2.新增数据到素材表中
            WmMaterial wmMaterial=new WmMaterial();
            wmMaterial.setUserId(WmThreadLocalUtil.getWmUser().getId());
            wmMaterial.setType((short) 0);//图片类型
            wmMaterial.setUrl(path);
            wmMaterial.setIsCollection((short) 0);//全部，没有收藏
            wmMaterial.setCreatedTime(new Date());
            save(wmMaterial);

            //3.返回数据
            return ResponseResult.okResult(wmMaterial);

        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_IMAGE_FORMAT_ERROR);
        }
    }

    /**
     * 查询素材列表
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult list(WmMaterialDto dto) {
        //1参数校验
        dto.checkParam();
            //3.分页设置
        IPage<WmMaterial> page=new Page<>(dto.getPage(),dto.getSize());
            //4.查询操作
        LambdaQueryWrapper<WmMaterial> queryWrapper= Wrappers.lambdaQuery();

        //4.1 是否收藏
        if(dto.getIsCollection()==1){
            queryWrapper.eq(WmMaterial::getIsCollection,dto.getIsCollection());
        }

        //4.2 根据用户id去查询
        queryWrapper.eq(WmMaterial::getUserId,WmThreadLocalUtil.getWmUser().getId());

        //2.查询mapper接口
        IPage<WmMaterial> resultPage = page(page, queryWrapper);

        //5.返回数据
        ResponseResult result=new PageResponseResult(dto.getPage(),dto.getSize(), (int) resultPage.getTotal());
        result.setData(resultPage.getRecords());
        return result;
    }
}
