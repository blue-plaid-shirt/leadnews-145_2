package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.heima.common.aliyun.GreenImageScan;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.common.tess4j.Tess4jClient;
import com.heima.feign.ArticleFeignClient;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmSensitive;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.common.SensitiveWordUtil;
import com.heima.wemedia.mapper.WmChannelMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmSensitiveMapper;
import com.heima.wemedia.mapper.WmUserMapper;
import com.heima.wemedia.service.WmAutoScanService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WmAutoScanServiceImpl implements WmAutoScanService {

    @Autowired
    private WmNewsMapper wmNewsMapper;

    @Autowired
    private GreenImageScan greenImageScan;

    @Autowired
    private GreenTextScan greenTextScan;

    @Autowired
    private FileStorageService fileStorageService;
    
    @Autowired
    private ArticleFeignClient articleFeignClient;

    @Autowired
    private WmUserMapper wmUserMapper;

    @Autowired
    private WmChannelMapper wmChannelMapper;

    @Autowired
    private WmSensitiveMapper wmSensitiveMapper;

    @Autowired
    private Tess4jClient tess4jClient;



    /**
     * 文章审核
     *主方法
     * @param newsId 文章id
     */
    @Override
    @Async//当前方法是异步调用
    public void autoScanWmNews(Integer newsId) {
        if(newsId==null){
            return;
        }
        //1.查询自媒体文章
        WmNews wmNews = wmNewsMapper.selectById(newsId);
        if(wmNews==null){
            return;
        }

        //1.5 抽取内容中的图片和文本
        Map<String, Object> map = extractImageAndTextFromContent(wmNews);

        //1.6 自管理敏感词文本审核
        Boolean sensitivesFlag=  scanSensitivesText((String)map.get("content"),wmNews);
        if(!sensitivesFlag){
            return;
        }

        //2.审核文本
       Boolean textFlag= scanWmNewsText((String)map.get("content"),wmNews);
        if(!textFlag){
            return;
        }

        //3.审核图片
        Boolean imageFlag= scanWmNewsImage((List<String>) map.get("imageUrlList"),wmNews);
        if(!imageFlag){
            return;
        }
        //4.远程调用保存三剑客的feign
        Long articlId = saveArticleFromWmNews(wmNews);

        //5.修改wm_news
        if(articlId==null){
            return;
        }
        wmNews.setArticleId(articlId);
        updateWmNews(wmNews, (short) 9,"文章已审核通过");
    }

    /**
     * 第六个抽取 方法，自管理敏感词审核
     * @param content
     * @param wmNews
     * @return
     */
    private Boolean scanSensitivesText(String content, WmNews wmNews) {
        boolean flag=true;

        //2.获取敏感词数据库表
        List<WmSensitive> wmSensitiveList=wmSensitiveMapper.selectList(null);
        //3.抽取敏感词字段成为一个集合
        List<String> sensitivesList = wmSensitiveList.stream().map(wmSensitive -> wmSensitive.getSensitives()).collect(Collectors.toList());
        //1.调用DFA算法初始化敏感词库
        SensitiveWordUtil.initMap(sensitivesList);
        //3.定义审核文本
        String scanText=content+wmNews.getTitle()+wmNews.getLabels();
        //4.匹配敏感词库
        Map<String, Integer> map = SensitiveWordUtil.matchWords(scanText);
        //5.判断是否包含敏感词
        if(map.size()>0){
            updateWmNews(wmNews, (short) 2,"文本中包含了自管理敏感词汇");
            flag=false;
        }
        return flag;
    }

    /**
     * 第五个方法，保存三剑客数据同步
     * @param wmNews
     */
    private Long saveArticleFromWmNews(WmNews wmNews) {
        ArticleDto dto=new ArticleDto();
        BeanUtils.copyProperties(wmNews,dto);
        //作者id
        dto.setAuthorId(wmNews.getUserId().longValue());
        //作者名称
        WmUser wmUser = wmUserMapper.selectById(wmNews.getUserId());
        if(wmUser!=null){
            dto.setAuthorName(wmUser.getName());
        }

        //频道名称
        WmChannel wmChannel = wmChannelMapper.selectById(wmNews.getChannelId());
        if(wmChannel!=null){
            dto.setChannelName(wmChannel.getName());
        }

        //文章布局
        dto.setLayout(wmNews.getType());

        //创建时间
        dto.setCreatedTime(new Date());
        ResponseResult result = articleFeignClient.saveArticle3(dto);
        if(result.getCode()!=200){
            return null;
        }
        return Long.parseLong(result.getData()+"");
    }

    /**
     * 抽取的第四个方法，审核图片
     * @param imageUrlList
     * @param wmNews
     * @return
     */
    private Boolean scanWmNewsImage(List<String> imageUrlList, WmNews wmNews) {
        boolean flag=true;
        try {
        //1.获取封面图片
        String images = wmNews.getImages();//注意，当前图片中有可能有多张图片，以逗号间隔，“xx.jpg,yy.jpg”
        String[] imageSplit = images.split(",");

        //把图片地址进行整合到一个list中
        imageUrlList.addAll(Arrays.asList(imageSplit));
        //去重
        imageUrlList= imageUrlList.stream().distinct().collect(Collectors.toList());


        List<byte[]> imageList=new ArrayList<>();
        //下载图片资源
        for (String path : imageUrlList) {
            byte[] bytes = fileStorageService.downLoadFile(path);

            //图片文字识别审核
            ByteArrayInputStream inputStream=new ByteArrayInputStream(bytes);
            BufferedImage bufferImage= ImageIO.read(inputStream);
            String resultText = tess4jClient.doOCR(bufferImage);

            //调用自管理敏感词审核
            Boolean sensitivesFlag=  scanSensitivesText(resultText,wmNews);
            if(!sensitivesFlag){
              return  false;
            }


            imageList.add(bytes);
        }
        //2.审核图片
            Map map = greenImageScan.imageScan(imageList);
            if(map!=null){
                if(map.get("suggestion").equals("block")){//失败
                    //修改wm_news表的状态
                    updateWmNews(wmNews, (short) 2,"图片中包含了敏感内容");
                    flag=false;
                }
                else if(map.get("suggestion").equals("review")){//人工
                    //修改wm_news表的状态
                    updateWmNews(wmNews, (short) 3,"图片中包含了不确定内容，需要人工审核");
                    flag=false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            flag=false;
        }
        return flag;

    }

    /**
     * 抽取的第二个方法，抽取内容中的图片和文本
     * @param wmNews
     */
    private Map<String,Object> extractImageAndTextFromContent(WmNews wmNews) {

        //定义一个字符串来统一管理文本
        StringBuilder sb=new StringBuilder();

        //定义一个List集合用来承载抽取出来的图片地址
        List<String> imageUrlList=new ArrayList<>();

        //1.获取title
        String title=wmNews.getTitle();
        sb.append(title);
        //2.获取标签
        String labels = wmNews.getLabels();
        sb.append(labels);

        //3.获取文章内容并抽取图片和文本
        String content = wmNews.getContent();
        List<Map> contentList = JSONArray.parseArray(content, Map.class);
        for (Map map : contentList) {
            if(map.get("type").equals("text")){//文本
                sb.append(map.get("value"));
            }
            if(map.get("type").equals("image")){//图片
               String imageUrl= (String) map.get("value");
               imageUrlList.add(imageUrl);
            }
        }
        Map<String,Object> resultMap=new HashMap<>();
        resultMap.put("content",sb.toString());
        resultMap.put("imageUrlList",imageUrlList);
        return resultMap;
    }

    /**
     * 第一个抽取方法，审核文本
     * @param content
     * @return
     */
    private Boolean scanWmNewsText(String content,WmNews wmNews) {
        boolean flag=true;
        //1.审核文本
        try {
            Map map = greenTextScan.greeTextScan(content);
            if(map!=null){
                if(map.get("suggestion").equals("block")){//失败
                    //修改wm_news表的状态
                    updateWmNews(wmNews, (short) 2,"文本中包含了敏感词汇");
                   flag=false;
                }
                else if(map.get("suggestion").equals("review")){//人工
                    //修改wm_news表的状态
                    updateWmNews(wmNews, (short) 3,"文本中包含了不确定词汇，需要人工审核");
                  flag=false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            flag=false;
        }
        return  flag;

    }

    /**
     * 第三个抽取方法，修改wm_news表的状态
     */
    private void updateWmNews(WmNews wmNews,Short status,String message) {
        wmNews.setStatus(status);
        wmNews.setReason(message);
        wmNewsMapper.updateById(wmNews);
    }
}
