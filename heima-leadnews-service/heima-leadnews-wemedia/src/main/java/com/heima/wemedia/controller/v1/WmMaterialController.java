package com.heima.wemedia.controller.v1;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.dtos.WmMaterialDto;
import com.heima.wemedia.service.WmMaterialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/material")
public class WmMaterialController {

    @Autowired
    private WmMaterialService wmMaterialService;
    /**
     * 图片上传
     * @param file
     * @return
     */
    @PostMapping("/upload_picture")
    public ResponseResult uploadImage(@RequestParam("multipartFile") MultipartFile file){
        return wmMaterialService.uploadImage(file);
    }

    /**
     * 查询素材列表
     * @param dto
     * @return
     */
    @PostMapping("/list")
    public ResponseResult list(@RequestBody WmMaterialDto dto){
        return wmMaterialService.list(dto);
    }
}
