package com.heima.wemedia.service.impl;

import com.heima.feign.IScheduleClient;
import com.heima.model.schedule.dtos.TaskDto;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.utils.common.ProtostuffUtil;
import com.heima.wemedia.service.WmAutoScanService;
import com.heima.wemedia.service.WmNewsTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;
@Service
@Slf4j
public class WmNewsTaskServiceImpl implements WmNewsTaskService {

    @Autowired
    private IScheduleClient scheduleClient;

    @Autowired
    private WmAutoScanService wmAutoScanService;
    /**
     * 添加任务到延迟队列中
     *
     * @param newsId      文章的id
     * @param publishTime 发布的时间  可以做为任务的执行时间
     */
    @Override
    @Async
    public void addNewsToTask(Integer newsId, Date publishTime) {

        log.info("添加任务到延迟服务中----begin");
        TaskDto dto=new TaskDto();
        dto.setTaskType(50);//类型
        dto.setPriority(100);//优先级
        dto.setExecuteTime(publishTime.getTime());//执行时间=发布时间

        WmNews wmNews=new WmNews();
        wmNews.setId(newsId);
        dto.setParameters(ProtostuffUtil.serialize(wmNews));//TODO
        //1.调feign接口
        scheduleClient.addTask(dto);
        log.info("添加任务到延迟服务中----begin");
    }

    /**
     * 拉取任务并文章审核
     */
    @Scheduled(fixedRate = 50000)//设置为50s，要不时间太短
    public void scanNewsByTask(){
        System.out.println("开始执行定时消费任务");
        //1.拉取延迟任务
        TaskDto dto = scheduleClient.poll(50, 100);
        if(dto==null){
            throw new RuntimeException("数据为空");
        }
        byte[] bytes = dto.getParameters();
        WmNews wmNews = ProtostuffUtil.deserialize(bytes, WmNews.class);

        //2.进行文章审核
        wmAutoScanService.autoScanWmNews(wmNews.getId());


        System.out.println("结束执行定时消费任务");
    }
}
