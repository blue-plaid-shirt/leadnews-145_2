package com.heima.wemedia.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.PageResponseResult;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.wemedia.dtos.WmNewsDto;
import com.heima.model.wemedia.dtos.WmNewsPageReqDto;
import com.heima.model.wemedia.pojos.WmMaterial;
import com.heima.model.wemedia.pojos.WmNews;
import com.heima.model.wemedia.pojos.WmNewsMaterial;
import com.heima.utils.threadlocal.WmThreadLocalUtil;
import com.heima.wemedia.mapper.WmMaterialMapper;
import com.heima.wemedia.mapper.WmNewsMapper;
import com.heima.wemedia.mapper.WmNewsMaterialMapper;
import com.heima.wemedia.service.WmAutoScanService;
import com.heima.wemedia.service.WmNewsService;
import com.heima.wemedia.service.WmNewsTaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class WmNewsServiceImpl extends ServiceImpl<WmNewsMapper, WmNews> implements WmNewsService {

    @Resource
    private WmNewsMaterialMapper wmNewsMaterialMapper;

    @Resource
    private WmMaterialMapper wmMaterialMapper;

    @Autowired
    private WmNewsTaskService wmNewsTaskService;

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;




    /**
     * 查询文章
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult findAll(WmNewsPageReqDto dto) {
        //1.参数校验
            //分页判断
        dto.checkParam();

        //2.分页查询wmnews
            //2.1分页条件设置
        IPage<WmNews> page=new Page<>(dto.getPage(),dto.getSize());
            //2.2 条件设置
        LambdaQueryWrapper<WmNews> queryWrapper= Wrappers.lambdaQuery();
            //2.3 判断文章状态
        if(dto.getStatus()!=null){
            queryWrapper.eq(WmNews::getStatus,dto.getStatus());
        }
            //2.4 关键字模糊查询
        if(dto.getKeyword()!=null){
            queryWrapper.like(WmNews::getTitle,dto.getKeyword());
        }
            //2.5 频道id
        if(dto.getChannelId()!=null){
            queryWrapper.eq(WmNews::getChannelId,dto.getChannelId());
        }

            //2.6 时间判断
        if(dto.getBeginPubdate()!=null && dto.getEndPubdate()!=null){
            queryWrapper.between(WmNews::getPublishTime,dto.getBeginPubdate(),dto.getEndPubdate());
        }
            //2.7 根据用户id
        queryWrapper.eq(WmNews::getUserId, WmThreadLocalUtil.getWmUser().getId());

        IPage<WmNews> resultPage = page(page, queryWrapper);

        //3.返回数据
        ResponseResult result=new PageResponseResult(dto.getPage(),dto.getSize(), (int) resultPage.getTotal());
        result.setData(resultPage.getRecords());

        return result;
    }

    /**
     * 文章发布(提交保存，提交修改，保存草稿)
     *主方法
     * @param dto
     * @return
     */
    @Override
    public ResponseResult submit(WmNewsDto dto) {
        //1.判断条件
        if(dto.getContent()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        if(dto.getType()==-1){
            dto.setType(null);
        }
        //2.执行修改或者新增操作
        WmNews wmNews=new WmNews();
        BeanUtils.copyProperties(dto,wmNews);
        //封面图片没有过来
        List<String> images = dto.getImages();
        if(images !=null && images.size()>0 ){
            wmNews.setImages(StringUtils.join(images,","));
        }
        saveOrUpdateWmNews(wmNews);

        //3.如果是草稿的话，终止
        if(dto.getStatus()==0){
            return ResponseResult.okResult(null);
        }

        //4.如果不是草稿的话，则执行中间表的操作
            //4.0 从文章内容中抽取出内容图片
        List<String> contentImageUrlList = extractContentImage(wmNews.getContent());
        //4.1 内容图片与文章的关系
            this.extractSaveRelation(wmNews,contentImageUrlList, (short) 0);
            //4.2 封面图片与文章的关系
            extractImagesToWmNewsMaterail(dto,wmNews,contentImageUrlList, (short) 1);


            //5. 调用自动审核业务
        //wmAutoScanService.autoScanWmNews(wmNews.getId());
        //5.异步调用
        wmNewsTaskService.addNewsToTask(wmNews.getId(),wmNews.getPublishTime());

        return ResponseResult.okResult("发布成功");
    }



    /**
     * 第四个抽取的方法，
     * @param wmNews
     * @param contentImageUrlList
     */
    public void extractSaveRelation(WmNews wmNews,  List<String> contentImageUrlList,Short type){
        if(contentImageUrlList!=null && contentImageUrlList.size()>0){
            //需要根据url地址去获取对应的素材id  select * from wm_material where url in(xx.jpg,xx.jpg)
            List<WmMaterial> wmMaterials = wmMaterialMapper.selectList(Wrappers.<WmMaterial>lambdaQuery().in(WmMaterial::getUrl, contentImageUrlList));
            List<Integer> contentImageIdList = wmMaterials.stream().map(item -> item.getId()).collect(Collectors.toList());
            //4.1 内容图片与文章的关系
            wmNewsMaterialMapper.saveRelations(contentImageIdList,wmNews.getId(), type);//0表示内容引用
        }
    }

    /**
     * 第三个抽取的方法，封面图片与文章的关系
     *     dto.getImages()  list中至少有一条数据
     *              有可能是多图   list中只有有三条数据
     *               有可能是无图  list中没有数据
     *               有可能是自动 ，需要从内容图片中去获取
     *                   如果内容中的图片数量大于=1，小于3,则认为是单图效果，从内容图片中取出第一张作为封面图片
     *                   如果内容中的图片数量大于等于3，则认为是多图，从内容图片中取前三张作为封面图片
     *                   如果内容中没有图片，则认为无图
     *
     */

    private void extractImagesToWmNewsMaterail(WmNewsDto dto,WmNews wmNews,  List<String> contentImageUrlList,Short type ) {
        //1.获取图片
        List<String> images = dto.getImages();

        //如果说是自动的话
        if(dto.getType()==null) {
            //单图设置
            if (contentImageUrlList.size() >= 1 && contentImageUrlList.size() < 3) {
                //取出第一张作为封面图片
                images = contentImageUrlList.stream().limit(1).collect(Collectors.toList());
                wmNews.setType((short) 1);//单图
            }
            //多图设置
           else if(contentImageUrlList.size()>=3){
                images = contentImageUrlList.stream().limit(3).collect(Collectors.toList());
                wmNews.setType((short) 3);
            }
            //无图设置
            else {
                wmNews.setType((short) 0);
            }
            wmNews.setImages(StringUtils.join(images,","));
            updateById(wmNews);
        }


        //2.写入数据
        this.extractSaveRelation(wmNews,images,type);
    }

    /**
     * 第二个抽取的方法，目的文章内容中抽取出内容图片
     * @param content
     */
    private List<String> extractContentImage(String content) {

        List<String> list=new ArrayList<>();

        //转化格式map
        List<Map> contentList = JSONArray.parseArray(content, Map.class);
        for (Map map : contentList) {
            if(map.get("type").equals("image")){
                String imageUrl = (String) map.get("value");
                list.add(imageUrl);
            }
        }

        return list;
    }

    /**
     * 第一个抽取的方法，新增和修改的业务，
     */
    private void saveOrUpdateWmNews(WmNews wmNews) {

        //填充数据
        wmNews.setCreatedTime(new Date());
        wmNews.setSubmitedTime(new Date());
        wmNews.setUserId(WmThreadLocalUtil.getWmUser().getId());


            //判断是否包含id
        if(wmNews.getId()!=null){
            //如果有id,则是修改
                //删除中间表的信息
                wmNewsMaterialMapper.delete(Wrappers.<WmNewsMaterial>lambdaQuery()
                .eq(WmNewsMaterial::getNewsId,wmNews.getId()));
                //修改wm_news表
                updateById(wmNews);
        }else{
            //如果没有id,则是新增
            //新增wm_news表
            save(wmNews);
        }
    }


    /**
     * 文章上下架
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult downOrUp(WmNewsDto dto) {
        //1.检查参数
        if(dto.getId() == null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }
        //2.查询文章
        WmNews wmNews = getById(dto.getId());
        if(wmNews==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //3.判断状态
        if(wmNews.getStatus()!=9){
            return ResponseResult.errorResult(AppHttpCodeEnum.WM_NEWS_STATUS_FAILE);
        }

        //4.修改文章
        wmNews.setEnable(dto.getEnable());
        updateById(wmNews);

        //5.发送消息
        Map map=new HashMap();
        map.put("articleId",wmNews.getArticleId());
        map.put("enable",wmNews.getEnable());
        kafkaTemplate.send("down_or_up_topic",JSON.toJSONString(map));


        return ResponseResult.okResult(AppHttpCodeEnum.SUCCESS);
    }



}
