package com.heima.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.dtos.LoginDto;
import com.heima.model.user.pojos.ApUser;
import com.heima.user.mapper.ApUserMapper;
import com.heima.user.service.ApUserService;
import com.heima.utils.common.AppJwtUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class ApUserServiceImpl extends ServiceImpl<ApUserMapper, ApUser> implements ApUserService {
    
   /* @Autowired
    private ApUserMapper apUserMapper;*/
    /**
     * 登录业务接口
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult login(LoginDto dto) {
        Map resultMap=new HashMap();

        //如果说是登录操作
        if(StringUtils.isNotBlank(dto.getPassword()) && StringUtils.isNotBlank(dto.getPhone())){
            //根据用户名查询数据库表
            ApUser apUser = getOne(Wrappers.<ApUser>lambdaQuery().eq(ApUser::getPhone, dto.getPhone()));
            if(apUser==null){
                //如果失败，则返回错误信息
                return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
            }
            //如果数据存在，则校验密码
                //获取salt
            String salt = apUser.getSalt();
                //对前台接收过来的密码进行加密
            String pwd = DigestUtils.md5DigestAsHex((dto.getPassword() + salt).getBytes());
                //校验密码
            if(!apUser.getPassword().equals(pwd)){//密码不一致
                return ResponseResult.errorResult(AppHttpCodeEnum.LOGIN_PASSWORD_ERROR);
            }
            //如果校验成功，则返回token和apuser
            resultMap.put("token", AppJwtUtil.getToken(apUser.getId().longValue()));
            apUser.setPassword("");
            apUser.setSalt("");
            resultMap.put("user",apUser);

        }else{
            //如果说是非登录操作
            //返回token, 0
            resultMap.put("token",AppJwtUtil.getToken(0l));
        }

        return ResponseResult.okResult(resultMap);
    }
}
