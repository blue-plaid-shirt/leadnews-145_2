package com.heima.user.controller.v1;

import com.heima.model.user.pojos.ApUser;
import com.heima.user.service.ApUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/user")
public class UserController {

    @Autowired
    private ApUserService apUserService;

    /**
     * 根据用户id去获取用户信息
     * @param userId
     * @return
     */
    @GetMapping("/findById/{userId}")
    public ApUser findById(@PathVariable Integer userId){
        ApUser apUser = apUserService.getById(userId);
        return apUser;
    }
}
