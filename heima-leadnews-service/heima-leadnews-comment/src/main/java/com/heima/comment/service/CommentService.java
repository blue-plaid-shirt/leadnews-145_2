package com.heima.comment.service;

import com.heima.comment.dtos.CommentLikeDto;
import com.heima.comment.dtos.CommentListDto;
import com.heima.comment.dtos.CommentSaveDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface CommentService {

    /**
     * 发表评论
     * @param dto
     * @return
     */
    public ResponseResult saveComment( CommentSaveDto dto);

    /**
     * 点赞评论
     * @param dto
     * @return
     */
    public ResponseResult likeComment( CommentLikeDto dto);


    /**
     * 查询评论列表集合
     * @param dto
     * @return
     */
    public ResponseResult loadComment( CommentListDto dto);
}
