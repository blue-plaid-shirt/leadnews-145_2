package com.heima.comment.controller.v1;

import com.heima.comment.dtos.CommentRepayDto;
import com.heima.comment.dtos.CommentRepayLikeDto;
import com.heima.comment.dtos.CommentRepayListDto;
import com.heima.comment.service.CommentRepayService;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 评论回复
 */
@RestController
@RequestMapping("/api/v1/comment_repay")
public class CommentRepayController {

    @Autowired
    private CommentRepayService commentRepayService;

    /**
     * 查询评论回复列表
     * @param dto
     * @return
     */
    @PostMapping("/load")
    public ResponseResult loadCommentRepay(@RequestBody CommentRepayListDto dto) {
        return commentRepayService.load(dto);
    }

    /**
     * 发送评论回复
     * @param dto
     * @return
     */
    @PostMapping("/save")
    public ResponseResult saveCommentRepay(@RequestBody CommentRepayDto dto) {
        return commentRepayService.saveCommentRepay(dto);
    }

    /**
     * 点赞评论回复
     * @param dto
     * @return
     */
    @PostMapping("/like")
    public ResponseResult saveCommentRepayLike(@RequestBody CommentRepayLikeDto dto) {
        return commentRepayService.like(dto);
    }

}