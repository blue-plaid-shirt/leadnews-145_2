package com.heima.comment.controller.v1;

import com.heima.comment.dtos.CommentLikeDto;
import com.heima.comment.dtos.CommentListDto;
import com.heima.comment.dtos.CommentSaveDto;
import com.heima.comment.service.CommentService;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/comment")
public class CommentController {

    @Autowired
    private CommentService commentService;

    /**
     * 发表评论
     * @param dto
     * @return
     */
    @PostMapping("/save")
    public ResponseResult saveComment(@RequestBody CommentSaveDto dto){
        return commentService.saveComment(dto);
    }

    /**
     * 点赞评论
     * @param dto
     * @return
     */
    @PostMapping("/like")
    public ResponseResult likeComment(@RequestBody CommentLikeDto dto){
        return commentService.likeComment(dto);
    }

    /**
     * 查询评论列表集合
     * @param dto
     * @return
     */
    @PostMapping("/load")
    public ResponseResult loadComment(@RequestBody CommentListDto dto){
        return commentService.loadComment(dto);
    }
}
