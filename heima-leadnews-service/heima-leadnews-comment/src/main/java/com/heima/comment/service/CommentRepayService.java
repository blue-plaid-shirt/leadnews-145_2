package com.heima.comment.service;

import com.heima.comment.dtos.CommentRepayDto;
import com.heima.comment.dtos.CommentRepayLikeDto;
import com.heima.comment.dtos.CommentRepayListDto;
import com.heima.model.common.dtos.ResponseResult;

public interface CommentRepayService {
    /**
     * 查询评论回复列表
     * @param dto
     * @return
     */
    ResponseResult load(CommentRepayListDto dto);
    /**
     * 发送评论回复
     * @param dto
     * @return
     */
    ResponseResult saveCommentRepay(CommentRepayDto dto);
    /**
     * 点赞评论回复
     * @param dto
     * @return
     */
    ResponseResult like(CommentRepayLikeDto dto);
}
