package com.heima.comment.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.comment.dtos.ApCommentVo;
import com.heima.comment.dtos.CommentLikeDto;
import com.heima.comment.dtos.CommentListDto;
import com.heima.comment.dtos.CommentSaveDto;
import com.heima.comment.pojos.ApComment;
import com.heima.comment.pojos.ApCommentLike;
import com.heima.comment.service.CommentService;
import com.heima.common.aliyun.GreenTextScan;
import com.heima.common.constants.HotArticleConstants;
import com.heima.feign.ApUserFeignClient;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.mess.UpdateArticleMess;
import com.heima.model.user.pojos.ApUser;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.query.UpdateDefinition;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ApUserFeignClient apUserFeignClient;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private GreenTextScan greenTextScan;

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;
    /**
     * 发表评论
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult saveComment(CommentSaveDto dto) {
        //1.检查参数
        if (dto.getArticleId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        if (dto.getContent() != null && dto.getContent().length() > 140) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE, "评论内容不能超过140字");
        }

        //2.判断是否登录
        ApUser apUser = ApThreadLocalUtil.getApUser();
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //2.5 判断如果是游客身份则不允许发表评论
        if(apUser.getId()==0){
            return  ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN,"请登录之后再来评论！！");
        }

        //3.先查询redis,如果不存在，把用户信息保存到redis
        //先查询redis
            //定义key
        String key="LOGIN_USER_"+apUser.getId();
        String apUserStr = redisTemplate.opsForValue().get(key);
        //说明不存在
        if(apUserStr==null){
            //远程调用用户信息
            apUser = apUserFeignClient.findById(apUser.getId());
            //保存数据到redis,设置过期时间2小时
            redisTemplate.opsForValue().set(key, JSON.toJSONString(apUser),2, TimeUnit.HOURS);
        }else {
            apUser=JSON.parseObject(apUserStr,ApUser.class);
        }


        //4.文本审核
        Boolean textFlag = this.scanContentText(dto);
        if(!textFlag) {
           return ResponseResult.errorResult(AppHttpCodeEnum.CONTENT_TEXT_SCAN_FAILE);
        }

        //5.保存数据到mongo中
        ApComment apComment=new ApComment() ;
        apComment.setUserId(apUser.getId());
        apComment.setNickName(apUser.getName());
        apComment.setContent(dto.getContent());
        apComment.setTargetId(dto.getArticleId().toString());
        apComment.setCreatedTime(new Date());
        apComment.setUpdatedTime(new Date());
        apComment.setImage(apUser.getImage());
        apComment.setLikes(0);
        apComment.setReply(0);
        mongoTemplate.save(apComment);


        UpdateArticleMess mess=new UpdateArticleMess();
        mess.setType(UpdateArticleMess.UpdateArticleType.COMMENT);
        mess.setArticleId(dto.getArticleId());
        mess.setAdd(1);
        //发送消息内容
        kafkaTemplate.send(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC, JSON.toJSONString(mess));

        //6.返回数据
        return ResponseResult.okResult("发表成功");
    }



    /**
     * 审核文本内容
     * @param dto
     * @throws Exception
     */
    private Boolean scanContentText(CommentSaveDto dto){
        boolean flag=true;
        //4.对评论内容做垃圾检测
        Map map = null;
        try {
            map = greenTextScan.greeTextScan(dto.getContent());
            if(map!=null) {
                if (!map.get("suggestion").equals("pass")) {//失败
                    flag=false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            flag=false;
        }
        return flag;
    }


    /**
     * 点赞评论
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult likeComment(CommentLikeDto dto) {
        //校验参数
        if (dto.getCommentId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }
        //2.判断是否登录
        ApUser apUser = ApThreadLocalUtil.getApUser();
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }
        //2.5 判断如果是游客身份则不允许发表评论
        if(apUser.getId()==0){
            return  ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN,"游客身份不能进行点赞操作！！");
        }

        //2.查询redis，并保存到mongo
            //定义redis的key
        String likeKey="COMMENT_LIKES_"+apUser.getId();
            //查询redis,参数1表示key,参数2表示value中的某一个值是否存在
        Boolean commentFlag = redisTemplate.opsForSet().isMember(likeKey, dto.getCommentId());


        //2.1 如果存在数据，且正在执行的是点赞操作
        if(commentFlag && dto.getOperation()==0){
            return ResponseResult.okResult("您已经点赞过该评论，请勿重复点赞");
        }
            //2.2 如果存在数据，且正在执行的是取消点赞
        if(commentFlag && dto.getOperation()==1){
            //删除redis
            redisTemplate.opsForSet().remove(likeKey,dto.getCommentId());
            //删除mongo评论点赞集合
            mongoTemplate.remove(
                    Query.query(Criteria.where("userId").is(apUser.getId()).and("targetId").is(dto.getCommentId())),
                    ApCommentLike.class
            );
            //更新mongo评论集合中的点赞数量 -1
            mongoTemplate.updateFirst(
                    Query.query(Criteria.where("id").is(dto.getCommentId())),
                    new Update().inc("likes",-1),
                    ApComment.class);
        }
        //2.3 如果不存在，则正在执行的是点赞操作
        if(!commentFlag && dto.getOperation()==0){
            //新增apcommentlike和更新apcomment
            ApCommentLike apCommentLike=new ApCommentLike();
            apCommentLike.setUserId(apUser.getId());
            apCommentLike.setCreatedTime(new Date());
            apCommentLike.setTargetId(dto.getCommentId());
            mongoTemplate.save(apCommentLike);

            mongoTemplate.updateFirst(
                    Query.query(Criteria.where("id").is(dto.getCommentId())),
                    new Update().inc("likes",1),
                    ApComment.class);

            //2.新增redis
            redisTemplate.opsForSet().add(likeKey,dto.getCommentId());
        }

        //3.返回数据
        ApComment apComment = mongoTemplate.findById(dto.getCommentId(), ApComment.class);
        Map map=new HashMap();
        map.put("likes",apComment.getLikes());

        return ResponseResult.okResult(map);
    }

    /**
     * 查询评论列表集合
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult loadComment(CommentListDto dto) {
        //1.检查参数
        if (dto.getArticleId() == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE);
        }

        if (dto.getSize() == null || dto.getSize() == 0) {
            dto.setSize(10);
        }
        dto.setSize(Math.min(dto.getSize(), 50));

        //2.根据当前文章id进行检索，按照发布时间倒序，分页查询（默认10条数据）
        List<ApComment> apCommentList = mongoTemplate.find(
                Query.query(Criteria.where("targetId").is(dto.getArticleId().toString()))
                        .with(Sort.by(Sort.Direction.DESC, "createdTime"))//排序
                        .limit(dto.getSize()),//分页
                ApComment.class
        );
        if(apCommentList==null && apCommentList.size()==0){
            return ResponseResult.errorResult(AppHttpCodeEnum.DATA_NOT_EXIST);
        }

        //3.对于用户点赞过的评论应该高亮展示
            //3.1 如果是游客身份，直接返回apCommentList
        ApUser apUser = ApThreadLocalUtil.getApUser();
        if (apUser == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN,"非法访问");
        }
        if(apUser.getId()==0){
            return  ResponseResult.okResult(apCommentList);
        }
            //3.2 非游客身份，是否高亮展示
        String likeKey="COMMENT_LIKES_"+apUser.getId();
        Set<String> membersSet = redisTemplate.opsForSet().members(likeKey);
        if(membersSet==null){//说明没有点赞过
            return  ResponseResult.okResult(apCommentList);
        }
        //否则，点赞过
        List<ApCommentVo> apCommentVoList = apCommentList.stream().map(apComment -> {
            //目的是为了重新生成一个list<ApCommentVO>
            ApCommentVo vo = new ApCommentVo();
            BeanUtils.copyProperties(apComment, vo);
            if (membersSet.contains(apComment.getId())) {//说明点赞过该评论
                vo.setOperation((short) 0);//表示点赞过
            }
            return vo;
        }).collect(Collectors.toList());

        return ResponseResult.okResult(apCommentVoList);
    }

}
