package com.heima.schedule.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.common.redis.RedisCacheService;
import com.heima.model.schedule.dtos.TaskDto;
import com.heima.model.schedule.pojos.Taskinfo;
import com.heima.model.schedule.pojos.TaskinfoLogs;
import com.heima.schedule.mapper.TaskinfoLogsMapper;
import com.heima.schedule.mapper.TaskinfoMapper;
import com.heima.schedule.service.TaskService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskinfoMapper taskinfoMapper;

    @Autowired
    private TaskinfoLogsMapper taskinfoLogsMapper;

    @Autowired
    private RedisCacheService redisCacheService;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;


    /**
     * 添加任务
     *
     * @param task 任务对象
     * @return 任务id
     */
    @Override
    public void addTask(TaskDto task) {


        threadPoolTaskExecutor.execute(new Runnable() {
            @Override
            public void run() {

                //1.添加任务到数据库表中，两张表
                boolean flag = addTaskToDB(task);

                //2.添加任务到redis中
                if(flag){
                    addTaskToRedis(task);
                }
            }
        });




    }


    /**
     * 抽取的第二个方法，添加任务到redis中
     * @param task
     */
    private void addTaskToRedis(TaskDto task) {

        //定义redis的key,list的key=prefKey+"_topic" ,zset的key=prefKey+"_future"
        String prefKey=task.getPriority()+"_"+task.getTaskType();

        //2.1 如果任务的执行时间小于等于当前系统时间，则存入到list队列中
        if(task.getExecuteTime()<=System.currentTimeMillis()){
            redisCacheService.lLeftPush(prefKey+"_topic", JSON.toJSONString(task));
        }
        //2.2 如果任务的执行时间大于当前时间，且小于等于预设时间(当前时间+5分钟)则存入zset队列中
        //定义未来五分钟的时间毫秒值
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.MINUTE,5);
        long futureTime=calendar.getTimeInMillis();
        if(task.getExecuteTime()>System.currentTimeMillis() && task.getExecuteTime()<=futureTime){
            /**
             * 三个参数，
             * 参数1表示key
             * 参数2表示value 值就是task
             * 参数3表示score分值，分值这次用执行时间作为分值
             */
            redisCacheService.zAdd(prefKey+"_future",JSON.toJSONString(task),task.getExecuteTime());
        }
    }

    /**
     * 抽取的方法，保存数据到数据库表中
     * @param task
     */
    private boolean addTaskToDB(TaskDto task) {

        //新增任务表
        Taskinfo taskinfo=new Taskinfo();
        BeanUtils.copyProperties(task,taskinfo);
        taskinfo.setExecuteTime(new Date(task.getExecuteTime()));
        taskinfoMapper.insert(taskinfo);

        //把taskinfo的id重新赋值给task参数
        task.setTaskId(taskinfo.getTaskId());


        //新增任务日志表
        TaskinfoLogs taskinfologs=new TaskinfoLogs();
        BeanUtils.copyProperties(taskinfo,taskinfologs);
        taskinfologs.setVersion(1);//初始化值
        taskinfologs.setStatus(0);//初始化0，不写也是mysql自动赋值0
        int insert = taskinfoLogsMapper.insert(taskinfologs);
        return insert==1;
    }


    /**
     * 拉取任务消费任务
     *   从list队列中获取任务，并删除任务
     * @param taskType
     * @param priority
     */
    @Override
    public TaskDto pullTask(Integer taskType, Integer priority) {

        Future<TaskDto> future = threadPoolTaskExecutor.submit(new Callable<TaskDto>() {
            @Override
            public TaskDto call() throws Exception {
                //1.获取key
                //定义redis的key,list的key=prefKey+"_topic" ,zset的key=prefKey+"_future"
                String prefKey = priority + "_" + taskType;

                //2.获取数据并删除从list队列中
                String taskStr = redisCacheService.lRightPop(prefKey + "_topic");
                TaskDto taskDto = JSON.parseObject(taskStr, TaskDto.class);

                //3.删除任务表以及修改任务日志表
                taskinfoMapper.deleteById(taskDto.getTaskId());
                //修改任务日志表
                TaskinfoLogs taskinfoLogs = taskinfoLogsMapper.selectById(taskDto.getTaskId());
                taskinfoLogs.setStatus(1);
                taskinfoLogsMapper.updateById(taskinfoLogs);

                return taskDto;
            }
        });

        //获取返回值结果
        TaskDto taskDto = null;
        try {
            taskDto = future.get(5, TimeUnit.SECONDS);
            return taskDto;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 定时刷新同步数据
     */
    @Override
    @Scheduled(cron = "*/1 * * * * ? ")
    public void refreshSync() {



        threadPoolTaskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                 /* String token = redisCacheService.tryLock("FUTURE_TASK_SYNC", 1000 * 30);
        if(StringUtils.isNotBlank(token)){*/


                System.out.println(System.currentTimeMillis() / 1000 + "执行了定时任务");

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //1.获取所有未来数据的keys
                Set<String> futureKeys = redisCacheService.scan("*_future");
                //根据所有的key获取value值
                for (String futureKey : futureKeys) { //futureKey= 50_100_future
                    Set<String> taskSet = redisCacheService.zRangeByScore(futureKey, 0, System.currentTimeMillis());
                    if(taskSet!=null && taskSet.size()>0){
                        //2.采用redis管道的方式同步到list中
                        String topicKey=futureKey.split("_future")[0]+"_topic";

                        /**
                         * 采用redis管道方式进行数据同步
                         * refreshWithPipeline(String future_key,String topic_key,Collection<String> values)
                         * 参数1 表示未来数据的key
                         * 参数2 表示list数据的key
                         * 参数3表示要进行同步的数据集合
                         *  表示从未来数据同步到list数据中
                         */
                        redisCacheService.refreshWithPipeline(futureKey,topicKey,taskSet);
                        System.out.println("成功的将" + futureKey + "下的当前需要执行的任务数据刷新到" + topicKey + "下");
                    }
                }
                //}
            }
        });



    }

    /**
     * 从数据库中的数据定时同步到redis
     */
    @Override
    //@Scheduled(cron = "0 0/5 * * * ? ")
    public void reloadData() {
        //1.清空redis
        Set<String> topicSet = redisCacheService.scan("*_topic");
        Set<String> futureSet = redisCacheService.scan("*_future");
        redisCacheService.delete(topicSet);
        redisCacheService.delete(futureSet);
        //2.查询未来五分钟以内的数据
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.MINUTE,5);
        List<Taskinfo> taskinfos = taskinfoMapper.selectList(Wrappers.<Taskinfo>lambdaQuery().lt(Taskinfo::getExecuteTime, calendar.getTimeInMillis()));
        //3.同步到redis
        for (Taskinfo taskinfo : taskinfos) {
            TaskDto taskDto=new TaskDto();
            BeanUtils.copyProperties(taskinfo,taskDto);
            taskDto.setExecuteTime(taskinfo.getExecuteTime().getTime());
            this.addTaskToRedis(taskDto);
        }

    }


}
