package com.heima.schedule.service;

import com.heima.model.schedule.dtos.TaskDto;

/**
 * 对外访问接口
 */
public interface TaskService {

    /**
     * 添加任务
     * @param task   任务对象
     * @return       任务id
     */
    public void addTask(TaskDto task) ;

    /**
     * 拉取任务消费任务
     * @param taskType
     * @param priority
     */
    public TaskDto pullTask(Integer taskType,Integer priority);

    /**
     * 定时刷新同步数据
     */
    public void refreshSync();

    /**
     * 从数据库中的数据定时同步到redis
     */
    public void reloadData();

}