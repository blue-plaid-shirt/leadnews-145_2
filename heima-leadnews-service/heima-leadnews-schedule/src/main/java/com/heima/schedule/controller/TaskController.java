package com.heima.schedule.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.schedule.dtos.TaskDto;
import com.heima.schedule.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    /**
     * 添加任务
     * @param dto
     * @return
     */
    @PostMapping("/api/v1/task/add")
    public ResponseResult addTask(@RequestBody TaskDto dto){
        taskService.addTask(dto);
        return ResponseResult.okResult("添加任务成功");
    }

    /**
     * 拉取任务
     * @param type
     * @param priority
     * @return
     */
    @GetMapping("/api/v1/task/poll/{type}/{priority}")
    public TaskDto poll(@PathVariable("type") int type, @PathVariable("priority")  int priority){
        TaskDto taskDto = taskService.pullTask(type, priority);
        return taskDto;
    }
}
