package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/*
文章列表mapper
 */
public interface ArticleHomeMapper extends BaseMapper<ApArticle> {
    /**
     * 查询文章列表
     * @param dto
     * @return
     */
    public List<ApArticle> loadArticle(@Param("dto") ArticleHomeDto dto, @Param("type") Short type);

    /**
     * 查询前五天的文章列表
     * @param last5Days
     * @return
     */
    public List<ApArticle> loadArticleLast5Days(@Param("last5Days") Date last5Days);
}
