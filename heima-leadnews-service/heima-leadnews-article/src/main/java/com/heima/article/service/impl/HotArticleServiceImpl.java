package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.heima.article.mapper.ArticleHomeMapper;
import com.heima.article.service.HotArticleService;
import com.heima.feign.WmChannelFeginClient;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.vos.HotArticleVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.wemedia.pojos.WmChannel;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class HotArticleServiceImpl implements HotArticleService {

    @Autowired
    private ArticleHomeMapper articleHomeMapper;

    @Autowired
    private WmChannelFeginClient wmChannelFeginClient;

    @Autowired
    private StringRedisTemplate redisTemplate;
    /**
     * 计算热点文章
     */
    @Override
    //4.定时器
    @XxlJob(value = "HotArticleJobHandle")
    public void computeHotArticle() {
        //1.查询前五天的文章列表
        Calendar calendar=Calendar.getInstance();
        calendar.add(Calendar.DATE,-5);
        Date last5Days=calendar.getTime();
        List<ApArticle> apArticleList = articleHomeMapper.loadArticleLast5Days(last5Days);

        //2.计算每篇文章的分值总和
        List<HotArticleVo> hotArticleVoList=jisuanArticleScore(apArticleList);

        //3.存入包含有分值的文章到redis中
        cacheTagToRedis(hotArticleVoList);

    }



    /**
     * 抽取的第三个方法，存入包含有分值的文章到redis中
     * @param hotArticleVoList
     */
    private void cacheTagToRedis(List<HotArticleVo> hotArticleVoList) {
        //1 根据频道进行存储
            //1.1 查询所有的频道
        ResponseResult<List<WmChannel>>result = wmChannelFeginClient.list();
        if(result.getCode()==200 && result.getData()!=null){
            //获取到所有的频道
            List<WmChannel> wmChannels = result.getData();
            //获取每个频道的下的所有的文章
            if(wmChannels!=null && wmChannels.size()>0){
                for (WmChannel channel : wmChannels) {
                    //获取每个频道下的所有的文章
                    List<HotArticleVo> hotArticleVos = hotArticleVoList.stream().filter(hotArticleVo -> hotArticleVo.getChannelId().equals(channel.getId())).collect(Collectors.toList());
                    //进行分值排序并取前30条
                    cacheToRedis(hotArticleVos, "hot_article_first_page_"+channel.getId());
                }
            }
        }
        //2 根据推荐全部进行存储
        cacheToRedis(hotArticleVoList, "hot_article_first_page_" + "__all__");
    }

    /**
     * 抽取的第四个方法，缓存数据到redis
     * @param hotArticleVoList
     * @param key
     */
    private void cacheToRedis(List<HotArticleVo> hotArticleVoList, String key) {
        //进行分值排序并取前30条
        hotArticleVoList = hotArticleVoList.stream().sorted(Comparator.comparing(HotArticleVo::getScore).reversed()).limit(30).collect(Collectors.toList());
        //存储数据到redis中
        redisTemplate.opsForValue().set(key, JSON.toJSONString(hotArticleVoList));
    }

    /**
     * 抽取的第一个方法，计算每篇文章的分值总和
     * @param apArticleList
     * @return
     */
    private List<HotArticleVo> jisuanArticleScore(List<ApArticle> apArticleList) {

        List<HotArticleVo> hotArticleVoList=new ArrayList<>();
        if(apArticleList!=null){
            //1.获取每一篇文章
            for (ApArticle apArticle : apArticleList) {
                HotArticleVo vo=new HotArticleVo();
                BeanUtils.copyProperties(apArticle,vo);
                //计算分值
                Integer score=computeHotArticleScore(apArticle);
                vo.setScore(score);
                hotArticleVoList.add(vo);
            }
        }
        return hotArticleVoList;
    }

    /**
     * 抽取的第二个方法，计算分值
     * @param apArticle
     * @return
     */
    private Integer computeHotArticleScore(ApArticle apArticle) {
        //先定义一个score
        Integer score=0;
        if(apArticle.getViews()!=null){
            score+=apArticle.getViews()*1;
        }
        if(apArticle.getLikes()!=null){
            score+=apArticle.getLikes()*3;
        }
        if(apArticle.getComment()!=null){
            score+=apArticle.getComment()*5;
        }
        if(apArticle.getCollection()!=null){
            score+=apArticle.getCollection()*8;
        }
        return score;
    }



    /**
     * 更新文章的分值  同时更新缓存中的热点文章数据
     *
     * @param mess
     */
    @Override
    public void updateScore(ArticleVisitStreamMess mess) {
        //1.更新ap_article数据库表
        ApArticle apArticle = articleHomeMapper.selectById(mess.getArticleId());
        if(apArticle!=null){
            apArticle.setCollection((apArticle.getCollection()==null?0:apArticle.getCollection())+mess.getCollect());
            apArticle.setComment((apArticle.getComment()==null?0:apArticle.getComment())+mess.getComment());
            apArticle.setViews((apArticle.getViews()==null?0:apArticle.getViews())+mess.getView());
            apArticle.setLikes((apArticle.getLikes()==null?0:apArticle.getLikes())+mess.getLike());
            articleHomeMapper.updateById(apArticle);
        }
        //2.重新计算分值
        Integer score = this.computeHotArticleScore(apArticle);
        score=score*3;

        //3.替换redis中数据
        //频道
        replaceRedis(apArticle,score,"hot_article_first_page_"+apArticle.getChannelId());
        //推荐
        replaceRedis(apArticle,score,"hot_article_first_page_"+"__all__");

    }

    /**
     * 抽取的替换redis中数据的方法
     * 以频道为基础
     */
    private void replaceRedis(ApArticle apArticle,Integer score,String key) {
        //1.查询redis中的数据
        String hotArticleVoListStr = redisTemplate.opsForValue().get(key);

        List<HotArticleVo> hotArticleVoList = JSONArray.parseArray(hotArticleVoListStr, HotArticleVo.class);

        boolean flag=true;

        if(hotArticleVoList!=null && hotArticleVoList.size()>0){
            //2.判断当前对象在缓存中是否存在
            for (HotArticleVo vo : hotArticleVoList) {
                //表示存在
                if(vo.getId().equals(apArticle.getId())){
                    vo.setScore(score);
                    flag=false;
                    break;
                }
            }
            //表示缓存不存在
            if(flag){
                if(hotArticleVoList.size()<30){
                    HotArticleVo hotArticleVo=new HotArticleVo();
                    BeanUtils.copyProperties(apArticle,hotArticleVo);
                    hotArticleVo.setScore(score);
                    hotArticleVoList.add(hotArticleVo);

                }else{

                    //取出最后一条
                    HotArticleVo lastHotArticleVo = hotArticleVoList.get(hotArticleVoList.size() - 1);
                    if(lastHotArticleVo.getScore()<score){
                        HotArticleVo hotArticleVo=new HotArticleVo();
                        BeanUtils.copyProperties(apArticle,hotArticleVo);
                        hotArticleVo.setScore(score);
                        hotArticleVoList.add(hotArticleVo);
                    }
                }

            }
            //直接添加到redis
            redisTemplate.opsForValue().set(key,JSON.toJSONString(hotArticleVoList));
        }

    }
}
