package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.HotArticleService;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * 重新计算分值实时计算的监听类
 */
@Component
public class ArticleIncrHandleListener {

    @Autowired
    private HotArticleService hotArticleService;


    @KafkaListener(topics = HotArticleConstants.HOT_ARTICLE_INCR_HANDLE_TOPIC)
    public void getMessage(String message){
        //1.解析message
        ArticleVisitStreamMess mess = JSON.parseObject(message, ArticleVisitStreamMess.class);
        //2.重新计算分值
        hotArticleService.updateScore(mess);
    }
}
