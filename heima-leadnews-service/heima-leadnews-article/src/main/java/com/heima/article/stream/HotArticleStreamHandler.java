package com.heima.article.stream;

import com.alibaba.fastjson.JSON;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.mess.UpdateArticleMess;
import org.apache.commons.lang.StringUtils;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * kafkastream执行聚合处理，流式处理的类
 *
 */
@Configuration
public class HotArticleStreamHandler {
    /**
     * 达到的目标是要求有一个字符串，包含articleId,和views阅读数量，likes点赞数量，collection收藏数量，comment评论数量
     * 接收kafka中的消息内容对象，包含了articleId，type,add
     * @param streamsBuilder
     * @return
     */
    @Bean
    public KStream<String,String> kStream(StreamsBuilder streamsBuilder){
        //接收消息内容
        KStream<String, String> kStream = streamsBuilder.stream(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC);
        //处理消息
        kStream.map(new KeyValueMapper<String, String, KeyValue<String,String>>() {
            /**
             * 重新定义key和value
             * @param key  表示消息的key,key=null
             * @param value 表示消息的内容，updateArticleMess对象的json字符串
             * @return  重新定义key和value
             *          key=文章id
             *          value=  type:add
             *                  views:1
             *                  likes:-1
             */
            @Override
            public KeyValue<String,String> apply(String key, String value) {
                //1.解析json
                UpdateArticleMess mess = JSON.parseObject(value, UpdateArticleMess.class);
                //2.重新定义key和value
                return new KeyValue<>(mess.getArticleId().toString(),mess.getType()+":"+mess.getAdd());
            }
        })
                /**
                 * key表示 文章id
                 * value=type:add
                 */
                .groupBy((key,value)->key)//根据文章id进行分组
                .windowedBy(TimeWindows.of(Duration.ofSeconds(10)))//时间窗口，每10s统计分析一次
                /**
                 * 开始聚合统计分析
                 */
                .aggregate(new Initializer<String>() {
                    /**
                     * 初始化操作
                     * 把当前收藏数量和品论数量，和点赞数量以及阅读数量进行初始化操作，并形成一个独立的字符串
                     */
                    @Override
                    public String apply() {
                        return "COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0";
                    }
                }, new Aggregator<String, String, String>() {
                               /**
                                * 统计分析业务
                                 * @param key 表示文章id
                                * @param value 表示type:add
                                * @param init 聚合字符串(初始化字符串)COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0
                                * @return
                                */
                    @Override
                    public String apply(String key, String value, String init) {
                            //1.判断value
                            if(StringUtils.isBlank(value)){
                                value=init;
                            }
                            //定义常量
                        int collection = 0,comment=0,like=0,view=0;
                            //2.对value进行切割字符串设置
                        String[] split = value.split(":");//[views,1]
                        switch (UpdateArticleMess.UpdateArticleType.valueOf(split[0])){
                            case COLLECTION:
                                collection+=Integer.parseInt(split[1]);
                                break;
                            case VIEWS:
                                view+=Integer.parseInt(split[1]);
                                break;
                            case LIKES:
                                like+=Integer.parseInt(split[1]);
                                break;
                            case COMMENT:
                                comment+=Integer.parseInt(split[1]);
                                break;
                        }
                        /**
                         * 字符串格式化的方法
                         * String format(String format, Object... args)
                         * 参数1表示字符串格式  %d表示占位符
                         * 参数2表示魏字符串中赋值的不定长度的参数
                         */
                        String formatStr=String.format("COLLECTION:%d,COMMENT:%d,LIKES:%d,VIEWS:%d",collection,comment,like,view);
                        return formatStr;
                    }
                },
                        /**
                         * 统计分析的名称命名，名字叫什么无所谓
                         */
                        Materialized.as("hot-article-count"))
                .toStream()
                //整理统计分析之后的结果，发送给下个topic
                .map(new KeyValueMapper<Windowed<String>, String, KeyValue<String, String>>() {
                    /**
                     * 整理统计分析之后的结果
                     * @param key 表示文章id   发送消息的key
                     * @param value 表示 COLLECTION:%d,COMMENT:%d,LIKES:%d,VIEWS:%d  发送的消息内容
                     * @return
                     */
                    @Override
                    public KeyValue<String, String> apply(Windowed<String> key, String value) {
                        return new KeyValue<>(key.key().toString(),format(key.key().toString(),value.toString()));
                    }
                })
                //发送消息
                .to(HotArticleConstants.HOT_ARTICLE_INCR_HANDLE_TOPIC);

        return kStream;

    }

    /**
     * 抽取的整合发送消息内容的方法
     * @param key 文章id
     * @param value COLLECTION:%d,COMMENT:%d,LIKES:%d,VIEWS:%d
     * @return  必须包含文章id和行为数量
     */
    public String format(String key,String value){
        ArticleVisitStreamMess mess=new ArticleVisitStreamMess();
        //封装数据
        mess.setArticleId(Long.parseLong(key));
        String[] split = value.split(",");//[COLLECTION:%d,COMMENT:%d,LIKES:%d,VIEWS:%d]
        for (String agg : split) {
            String[] split1 = agg.split(":");//[COLLECTION,%d]
            switch (UpdateArticleMess.UpdateArticleType.valueOf(split1[0])){
                case COLLECTION:
                    mess.setCollect(Integer.parseInt(split1[1]));
                    break;
                case VIEWS:
                    mess.setView(Integer.parseInt(split1[1]));
                    break;
                case LIKES:
                    mess.setLike(Integer.parseInt(split1[1]));
                    break;
                case COMMENT:
                    mess.setComment(Integer.parseInt(split1[1]));
                    break;
            }
        }
        return JSON.toJSONString(mess);
    }
}
