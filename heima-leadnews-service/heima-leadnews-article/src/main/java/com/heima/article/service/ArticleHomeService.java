package com.heima.article.service;

import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.web.bind.annotation.RequestBody;

public interface ArticleHomeService {
    /**
     *
     * @param dto
     * @param type 1表示上拉加载更多，2表示下拉加载更新
     * @return
     */
    public ResponseResult load(ArticleHomeDto dto,Short type);

    /**
     *首页，查询redis
     * @param dto
     * @return
     */
    public ResponseResult load2(ArticleHomeDto dto);


    /**
     * 保存三剑客
     * @param dto
     * @return
     */
    public ResponseResult saveArticle3( ArticleDto dto);
}
