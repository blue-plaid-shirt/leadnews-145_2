package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.mapper.ApArticleConfigMapper;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ArticleHomeMapper;
import com.heima.article.service.ArticleHomeService;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleConfig;
import com.heima.model.article.pojos.ApArticleContent;
import com.heima.model.article.vos.HotArticleVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.vos.SearchArticleVo;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticleHomeServiceImpl implements ArticleHomeService {
    
    @Autowired
    private ArticleHomeMapper articleHomeMapper;
    
    @Autowired
    private ApArticleConfigMapper apArticleConfigMapper;
    
    @Autowired
    private ApArticleContentMapper apArticleContentMapper;

    @Autowired
    private Configuration configuration;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    @Autowired
    private StringRedisTemplate redisTemplate;
    
    
    /**
     * @param dto
     * @param type 1表示上拉加载更多，2表示下拉加载更新
     * @return
     */
    @Override
    public ResponseResult load(ArticleHomeDto dto, Short type) {
        //校验参数
        if(dto.getSize()==0){
            dto.setSize(10);
        }
        if(dto.getTag()==null){
            dto.setTag("__all__");
        }
        if(dto.getMaxBehotTime()==null){
            dto.setMaxBehotTime(new Date());
        }
        if(dto.getMinBehotTime()==null){
            dto.setMinBehotTime(new Date());
        }
        //调用mapper接口
        List<ApArticle> apArticles = articleHomeMapper.loadArticle(dto, type);
        //返回数据
        return ResponseResult.okResult(apArticles);
    }

    /**
     * 首页，查询redis
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult load2(ArticleHomeDto dto) {
        //校验参数
        if(dto.getTag()==null){
            dto.setTag("__all__");
        }
        //查询redis
        String prfexKey="hot_article_first_page_";
        String hotArticleVoListStr = redisTemplate.opsForValue().get(prfexKey + dto.getTag());
        //转化格式
        List<HotArticleVo> hotArticleVoList = JSONArray.parseArray(hotArticleVoListStr, HotArticleVo.class);
        return ResponseResult.okResult(hotArticleVoList);
    }

    /**
     * 保存三剑客
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional
    public ResponseResult saveArticle3(ArticleDto dto) {
        //非空判断
        if(dto.getContent()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID,"文章内容不能为空");
        }
        ApArticle apArticle=new ApArticle();
        BeanUtils.copyProperties(dto,apArticle);

        ApArticleContent apArticleContent=new ApArticleContent();
        apArticleContent.setContent(dto.getContent());
        //如果是保存文章
        if(dto.getId()==null){
            //保存文章

            articleHomeMapper.insert(apArticle); //当前apArticle中是包含了id
            //保存文章配置表
            ApArticleConfig apArticleConfig=new ApArticleConfig(apArticle.getId());
            apArticleConfigMapper.insert(apArticleConfig);
            //保存文章内容表

            apArticleContent.setArticleId(apArticle.getId());
            apArticleContentMapper.insert(apArticleContent);
        }else{
            //如果是修改文章
            //修改文章表
            articleHomeMapper.updateById(apArticle);
            //修改文章内容表  update 表 set content=? where articleId=?
            apArticleContentMapper.update(apArticleContent, Wrappers.<ApArticleContent>lambdaUpdate().eq(ApArticleContent::getArticleId,apArticle.getId()));
        }

        //异步线程调用生成静态文件
        buildStaticHtmlToMinio(apArticle.getId());





        //返回id的目的是为了在wm_news表中articleId进行赋值操作
        return ResponseResult.okResult(apArticle.getId()==null?"":apArticle.getId());
    }

    /**
     * 生成静态文件
     */
    @Async
    public void buildStaticHtmlToMinio(Long articleId) {
        //1.获取模板文件
        Template template = null;
        try {
            template = configuration.getTemplate("article.ftl");

        //2.获取数据模型
        Map dataModel=new HashMap();
        //查询content数据库，根据文章id
        ApArticleContent content = apArticleContentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery().eq(ApArticleContent::getArticleId, articleId));

        dataModel.put("content", JSONArray.parseArray(content.getContent()));
        //3.生成静态文件
        StringWriter out=new StringWriter();//字符串写入流此时为空
        template.process(dataModel,out);//此时的字符串写入流中就有数据了

        //4.上传到minio中，返回路径地址
        InputStream inputStream=new ByteArrayInputStream(out.toString().getBytes());
        String path = fileStorageService.uploadHtmlFile("", content.getArticleId() + ".html", inputStream);


        //5.修改ap_article表的staticUrl
        ApArticle apArticle=new ApArticle();
        apArticle.setId(content.getArticleId());
        apArticle.setStaticUrl(path);
        articleHomeMapper.updateById(apArticle);


            //发送消息
            SearchArticleVo vo=new SearchArticleVo();
            BeanUtils.copyProperties(apArticle,vo);
            vo.setLayout(apArticle.getLayout().intValue());
            vo.setContent(content.getContent());

            kafkaTemplate.send("article_sync_es_topic", JSON.toJSONString(vo));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
