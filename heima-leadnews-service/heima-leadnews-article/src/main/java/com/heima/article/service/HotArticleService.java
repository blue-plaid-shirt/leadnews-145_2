package com.heima.article.service;

import com.heima.model.mess.ArticleVisitStreamMess;

/**
 * 热点文章业务接口
 */
public interface HotArticleService {
    /**
     * 计算热点文章
     */
    public void computeHotArticle();


    /**
     * 更新文章的分值  同时更新缓存中的热点文章数据
     * @param mess
     */
    public void updateScore(ArticleVisitStreamMess mess);
}
