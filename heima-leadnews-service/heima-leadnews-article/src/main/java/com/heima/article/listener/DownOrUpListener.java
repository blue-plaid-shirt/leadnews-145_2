package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.service.ArticleConfigService;
import com.heima.model.article.pojos.ApArticleConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 异步通知文章上下架
 */
@Component
public class DownOrUpListener {

    @Autowired
    private ArticleConfigService articleConfigService;

    /**
     * 监听kafka消息
     */
    @KafkaListener(topics = "down_or_up_topic")
    public void getMessage(String message){
        //1.转格式
        Map map = JSON.parseObject(message, Map.class);
        //2.获取文章id和上下架状态
        Long articleId = (Long) map.get("articleId");
        //1上架，0下架
        Integer enable = (Integer) map.get("enable");
        //3.修改状态
        articleConfigService.update(Wrappers.<ApArticleConfig>lambdaUpdate()
            .set(ApArticleConfig::getIsDown,enable==1?false:true)
                .eq(ApArticleConfig::getArticleId,articleId)
        );
    }
}
