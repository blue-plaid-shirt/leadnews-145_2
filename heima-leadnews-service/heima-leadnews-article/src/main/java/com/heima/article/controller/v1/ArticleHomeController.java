package com.heima.article.controller.v1;

import com.heima.article.service.ArticleHomeService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.web.bind.annotation.*;

/**
 * 文章列表查询业务
 */
@RestController
@RequestMapping("/api/v1/article")
public class ArticleHomeController {

    @Autowired
    private ArticleHomeService articleHomeService;
    /**
     * 加载首页
     * @param dto
     * @return
     */
    @PostMapping("/load")
    public ResponseResult load(@RequestBody ArticleHomeDto dto){
        //return articleHomeService.load(dto, (short) 3);
        return articleHomeService.load2(dto);
    }

    /**
     * 加载更多，上拉
     * @param dto
     * @return
     */
    @PostMapping("/loadmore")
    public ResponseResult loadmore(@RequestBody ArticleHomeDto dto){
        return articleHomeService.load(dto, (short) 1);
    }

    /**
     * 加载最新，下拉
     * @param dto
     * @return
     */
    @PostMapping("/loadnew")
    public ResponseResult loadnew(@RequestBody ArticleHomeDto dto){
        return articleHomeService.load(dto, (short) 2);
    }


    /**
     * 保存三剑客
     * @param dto
     * @return
     */
    @PostMapping("/save")
    public ResponseResult saveArticle3(@RequestBody ArticleDto dto){
        return articleHomeService.saveArticle3(dto);
    }
}
