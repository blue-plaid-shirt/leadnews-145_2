package com.heima.article;

import com.heima.file.service.FileStorageService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@SpringBootTest
@RunWith(SpringRunner.class)
public class MinioTest {

    @Resource
    FileStorageService fileStorageService;
    //上传图片
    @Test
    public void uploadImage() throws FileNotFoundException {
        InputStream inputStream=new FileInputStream("E:\\上课资料\\2021-新版课程资料\\08-项目三黑马头条v2.0\\day04-自媒体文章审核\\资料\\图片素材\\15.jpg");
        String path = fileStorageService.uploadImgFile("", "aa.jpg", inputStream);
        System.out.println(path);
    }
    //上传文件
    @Test
    public void uploadHtml() throws FileNotFoundException {
        InputStream inputStream=new FileInputStream("E:\\list.html");
        String path = fileStorageService.uploadHtmlFile("", "aa.html", inputStream);
        System.out.println(path);
    }

}
