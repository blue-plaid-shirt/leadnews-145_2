package com.heima.article;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ArticleHomeMapper;
import com.heima.file.service.FileStorageService;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleContent;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 测试文章详情静态化以及上传minio
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ArticleInfoTest {

    @Autowired
    private Configuration configuration;

    @Autowired
    private ApArticleContentMapper contentMapper;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ArticleHomeMapper articleHomeMapper;

    @Test
    public void generaterHtmlToMino() throws Exception {

        //1.获取模板文件
        Template template = configuration.getTemplate("article.ftl");
        //2.获取数据模型
        Map dataModel=new HashMap();
            //查询content数据库，根据文章id
        ApArticleContent content = contentMapper.selectOne(Wrappers.<ApArticleContent>lambdaQuery().eq(ApArticleContent::getArticleId, "1554473209301131265"));

        dataModel.put("content", JSONArray.parseArray(content.getContent()));
        //3.生成静态文件
        StringWriter out=new StringWriter();//字符串写入流此时为空
        template.process(dataModel,out);//此时的字符串写入流中就有数据了

        //4.上传到minio中，返回路径地址
        InputStream inputStream=new ByteArrayInputStream(out.toString().getBytes());
        String path = fileStorageService.uploadHtmlFile("", content.getArticleId() + ".html", inputStream);


        //5.修改ap_article表的staticUrl
        ApArticle apArticle=new ApArticle();
        apArticle.setId(content.getArticleId());
        apArticle.setStaticUrl(path);
        articleHomeMapper.updateById(apArticle);
    }
}
