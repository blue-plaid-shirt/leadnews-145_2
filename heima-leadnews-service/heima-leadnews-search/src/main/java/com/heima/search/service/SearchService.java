package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.search.vos.SearchArticleVo;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;

public interface SearchService {
    /**
     * 用户搜索
     * @param dto
     * @return
     */
    public ResponseResult search( UserSearchDto dto);

    /**
     * 写入数据同步到es
     * @param message
     */
    public void writerArticleToEs(String message);

    /**
     * 保存历史记录
     * @param keyword
     * @param userId
     */
    public void saveMongo(String keyword,Integer userId);
}
