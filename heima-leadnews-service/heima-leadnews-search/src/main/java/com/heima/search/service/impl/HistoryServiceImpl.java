package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.user.pojos.ApUser;
import com.heima.search.pojos.ApUserSearch;
import com.heima.search.pojos.HistorySearchDto;
import com.heima.search.service.HistoryService;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import com.mongodb.client.result.DeleteResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private MongoTemplate mongoTemplate;
    /**
     * 加载历史记录
     *
     * @return
     */
    @Override
    public ResponseResult loadHistory() {
        //2.从当前线程中获取用户id
        ApUser apUser = ApThreadLocalUtil.getApUser();
        if(apUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        //1.查询mongo
        List<ApUserSearch> apUserSearches = mongoTemplate.find(
                Query.query(Criteria.where("userId").is(apUser.getId())).with(Sort.by(Sort.Direction.DESC, "createdTime")),
                ApUserSearch.class
        );

        return ResponseResult.okResult(apUserSearches);
    }

    /**
     * 删除历史记录
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult delHistory(HistorySearchDto dto) {

        if(dto.getId()==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        ApUser apUser = ApThreadLocalUtil.getApUser();
        if(apUser==null){
            return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
        }

        DeleteResult deleteResult = mongoTemplate.remove(
                Query.query(Criteria.where("userId").is(apUser.getId()).and("id").is(dto.getId())),
                ApUserSearch.class
        );
        if(deleteResult.wasAcknowledged()){
            return ResponseResult.okResult("删除成功");
        }
        return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"删除失败");
    }
}
