package com.heima.search.listener;

import com.alibaba.fastjson.JSON;
import com.heima.model.search.vos.SearchArticleVo;
import com.heima.search.service.SearchService;
import org.apache.avro.data.Json;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * 文章同步es的监听类
 */
@Component
public class ArticleSyncEsListener {

    @Autowired
    private SearchService searchService;

    @KafkaListener(topics = "article_sync_es_topic")
    public void getMessage(String message){
        //2.写入到es中
        searchService.writerArticleToEs(message);
    }
}
