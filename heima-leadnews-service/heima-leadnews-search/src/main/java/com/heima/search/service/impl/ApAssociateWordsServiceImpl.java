package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ApAssociateWordsService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.suggest.Suggest;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.elasticsearch.search.suggest.SuggestBuilders;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ApAssociateWordsServiceImpl implements ApAssociateWordsService {

    @Autowired
    private RestHighLevelClient client;

    /**
     * 自动补全功能，关键字联想功能，使用es
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult search(UserSearchDto dto) {
        try {
        //2.创建搜索请求对象，并指定索引库名称
        SearchRequest searchRequest=new SearchRequest("app_info_article");

        //3.执行自动补全
        searchRequest.source().suggest(
                /**
                 * addSuggestion(String name, SuggestionBuilder<?> suggestion)
                 * 参数1表示自动补全的名称，为了结果封装的使用
                 * 参数2
                 */
               new SuggestBuilder().addSuggestion(
                       "mySuggest",
                       SuggestBuilders.completionSuggestion("suggestion")//指定搜索的域名
                       .prefix(dto.getSearchWords())//执行的搜索条件
                       .size(10)//最多展示10条
                       .skipDuplicates(true)//遇到重复的跳过
               )
        );

        //1.执行搜索
            SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);



            //4.结果封装
            Suggest suggest = response.getSuggest();
            CompletionSuggestion mySuggest = suggest.getSuggestion("mySuggest");

            List<CompletionSuggestion.Entry.Option> options = mySuggest.getOptions();

            //定义一个list集合用来封装数据
            List<String> list=new ArrayList<>(options.size());

            for (CompletionSuggestion.Entry.Option option : options) {
                Text text = option.getText();//获取自动补全内容
                list.add(text.toString());
            }
            return ResponseResult.okResult(list);

        } catch (IOException e) {
            e.printStackTrace();
            return ResponseResult.errorResult(AppHttpCodeEnum.SERVER_ERROR,"自动补全出现错误");
        }

    }
}
