package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

/**
自动补全功能，关键字联想功能，使用es
 *
 * @author 
 */
public interface ApAssociateWordsService {

   /**
     * 自动补全功能，关键字联想功能，使用es
     * @param dto
     * @return
     */
    public ResponseResult search(UserSearchDto dto);

}