package com.heima.search.config;

import com.heima.model.user.pojos.ApUser;
import com.heima.model.wemedia.pojos.WmUser;
import com.heima.utils.common.AppJwtUtil;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import com.heima.utils.threadlocal.WmThreadLocalUtil;
import io.jsonwebtoken.Claims;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

/**
 * 拦截器获取token,然后把用户信息封装到当前线程中
 */
public class ApTokenInterceptor implements HandlerInterceptor {
    /**
     * 请求任何方法之前执行
     * @param request
     * @param response
     * @param handler
     * @return   false表示拦截，true表示放行
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //1.从请求头中获取token
        String token = request.getHeader("token");
        Optional<String> optional = Optional.ofNullable(token);
        if(optional.isPresent()){//如果token有值
            //2.解析token
            Claims claimsBody = AppJwtUtil.getClaimsBody(token);//获取载荷
            Object id = claimsBody.get("id");

            //3.用户信息保存到threadLocal中
            ApUser apUser=new ApUser();
            apUser.setId(Integer.parseInt(id+""));
            ApThreadLocalUtil.setApUser(apUser);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        ApThreadLocalUtil.removeApUser();
    }
}
