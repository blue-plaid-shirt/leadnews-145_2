package com.heima.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.search.vos.SearchArticleVo;
import com.heima.model.user.pojos.ApUser;
import com.heima.search.pojos.ApUserSearch;
import com.heima.search.service.SearchService;
import com.heima.utils.threadlocal.ApThreadLocalUtil;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private MongoTemplate mongoTemplate;


    /**
     * 用户搜索
     *
     * @param dto
     * @return
     */
    @Override
    public ResponseResult search(UserSearchDto dto) {
        try {
        //1.1 创建请求搜索对象
        SearchRequest searchRequest=new SearchRequest("app_info_article");

        //1.2 条件查询，根据关键字查询
            //没有关键字，查询所有
        if(dto.getSearchWords()==null){
            searchRequest.source().query(QueryBuilders.matchAllQuery());
        }else if(dto.getSearchWords()!=null){
            searchRequest.source().query(QueryBuilders.matchQuery("all",dto.getSearchWords()));
        }
        //1.3 分页
        searchRequest.source().from(0).size(dto.getPageSize());

        //1.4 排序
        searchRequest.source().sort("publishTime", SortOrder.DESC);

        //1.5 高亮
        searchRequest.source().highlighter(
                new HighlightBuilder()
                        .field("title")
                        .preTags("<font style='color: red; font-size: inherit;'>")
                        .postTags("</font>")
                .requireFieldMatch(false)
        );
        //1.执行查询搜索
        SearchResponse   response = client.search(searchRequest, RequestOptions.DEFAULT);
            //3.定义一个list
            List<Map> list=new ArrayList<>();

            //2.处理搜索结果
            SearchHit[] hits = response.getHits().getHits();
            for (SearchHit hit : hits) {
                //获取source
                Map<String, Object> map = hit.getSourceAsMap();

                //取出高亮域
                HighlightField highlightField = hit.getHighlightFields().get("title");
                Text[] fragments = highlightField.getFragments();
                //替换title
                if(fragments.length>0){//有高亮
                    map.put("h_title",fragments[0].toString());
                }else{//没有高亮
                    map.put("h_title",map.get("title"));
                }
                list.add(map);
            }


            //异步调用保存历史记录
            ApUser apUser = ApThreadLocalUtil.getApUser();
            if(apUser!=null){
                this.saveMongo(dto.getSearchWords(),apUser.getId());
            }




            return ResponseResult.okResult(list);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_REQUIRE,"查询结果出错");
        }
    }

    /**
     * 写入数据同步到es
     *
     * @param message
     */
    @Override
    public void writerArticleToEs(String message) {
        //1.解析
        SearchArticleVo vo = JSON.parseObject(message, SearchArticleVo.class);
        //2.创建新增文档请求对象，并且指定具体操作的索引库名称
        IndexRequest indexRequest=new IndexRequest("app_info_article").id(vo.getId().toString());
        //3.添加数据
        indexRequest.source(message, XContentType.JSON);
        //1.调用es的api新增文档
        try {
            client.index(indexRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存历史记录
     *
     * @param keyword
     * @param userId
     */
    @Override
    @Async
    public void saveMongo(String keyword, Integer userId) {
        //1.根据关键词和用户id查询mongo
        ApUserSearch apUserSearch = mongoTemplate.findOne(
                Query.query(Criteria.where("userId").is(userId).and("keyword").is(keyword)),
                ApUserSearch.class
        );
        //2.如果存在，则更新时间
        if(apUserSearch!=null){
            apUserSearch.setCreatedTime(new Date());
            mongoTemplate.save(apUserSearch);
            return;
        }

        //3.如果不存在
            //3.1 根据当前用户id查询mongo,返回list集合
        List<ApUserSearch> apUserSearchList = mongoTemplate.find(
                Query.query(Criteria.where("userId").is(userId)).with(Sort.by(Sort.Direction.DESC,"createdTime")),
                ApUserSearch.class
        );
    //封装最新数据
        apUserSearch = new ApUserSearch();
        apUserSearch.setUserId(userId);
        apUserSearch.setKeyword(keyword);
        apUserSearch.setCreatedTime(new Date());

        //3.2 判断长度是否大于10
        if(apUserSearchList==null || apUserSearchList.size()<10){
            //3.3 如果不大于10，则直接新增
            mongoTemplate.save(apUserSearch);
        }else{
            //3.4 如果大于10，则替换最后一条数据
            ApUserSearch oldLastApUserSearch = apUserSearchList.get(apUserSearchList.size() - 1);


            /**
             * findAndReplace(Query query, T replacement)
             * 两个参数，参数1表示被替换的元素
             * 参数2表示替换的新元素
             */
            mongoTemplate.findAndReplace(
                    Query.query(Criteria.where("id").is(oldLastApUserSearch.getId())),
                    apUserSearch
            );
        }

    }
}
