package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.pojos.HistorySearchDto;

public interface HistoryService {
    /**
     * 加载历史记录
     * @return
     */
    public ResponseResult loadHistory();

    /**
     * 删除历史记录
     * @param dto
     * @return
     */
    ResponseResult delHistory(HistorySearchDto dto);
}
