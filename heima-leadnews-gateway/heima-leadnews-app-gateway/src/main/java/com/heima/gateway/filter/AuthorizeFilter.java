package com.heima.gateway.filter;

import com.heima.gateway.utils.AppJwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * app认证过滤器，目的是为了所有的请求都要通过网关，网关就要判断是否携带token
 */
@Component
public class AuthorizeFilter implements GlobalFilter, Ordered {
    /**
     * 执行的过滤器方法
     * @param exchange  交换机，可以获取请求对象和响应对象
     * @param chain 过滤器链
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        //0.获取请求对象和响应对象
        ServerHttpRequest request = exchange.getRequest();
        ServerHttpResponse response = exchange.getResponse();

        //1.判断是否是登录请求
        if(request.getURI().getPath().contains("/login/login_auth")){
            //如果是，则放行
            return chain.filter(exchange);//放行
        }
        //非登录操作
        //如果不是，则检查请求头中是否有token
        String token = request.getHeaders().getFirst("token");
        if(token==null){//报401
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }
            //如果有值，则判断是token否有效
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        int i = AppJwtUtil.verifyToken(claimsBody);
        if(i==1 || i==2){//无效
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //最后放行
        return chain.filter(exchange);
    }

    /**
     * 返回值越小，优先级越高
     * @return
     */
    @Override
    public int getOrder() {
        return 0;
    }
}
