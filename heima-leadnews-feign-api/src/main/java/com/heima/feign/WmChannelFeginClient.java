package com.heima.feign;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient("leadnews-wemedia")
public interface WmChannelFeginClient {
    /**
     * 查询所有的频道
     * @return
     */
    @GetMapping("/api/v1/channel/channels")
    public ResponseResult<List<WmChannel>> list();
}
