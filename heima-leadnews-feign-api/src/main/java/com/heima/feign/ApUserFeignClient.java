package com.heima.feign;

import com.heima.model.user.pojos.ApUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("leadnews-user")
public interface ApUserFeignClient {
    /**
     * 根据用户id查询用户信息
     * @param userId
     * @return
     */
    @GetMapping("/api/v1/user/findById/{userId}")
    public ApUser findById(@PathVariable("userId") Integer userId);
}
